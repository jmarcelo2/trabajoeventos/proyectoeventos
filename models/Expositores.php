<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "expositores".
 *
 * @property string $codigoExpositor
 * @property string|null $dniExpositor
 * @property string|null $nombreExpositor
 * @property string|null $nacionalidad
 * @property string|null $genero
 * @property string|null $correo
 * @property string|null $fechaNacimiento
 * @property string|null $telefono
 *
 * @property Exponenconferencias[] $exponenconferencias
 * @property Conferencias[] $idConferencias
 */
class Expositores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'expositores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigoExpositor'], 'required'],
            [['fechaNacimiento'], 'safe'],
            [['codigoExpositor', 'dniExpositor', 'telefono'], 'string', 'max' => 9],
            [['nombreExpositor', 'nacionalidad', 'correo'], 'string', 'max' => 50],
            [['genero'], 'string', 'max' => 10],
            [['dniExpositor'], 'unique'],
            [['codigoExpositor'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigoExpositor' => 'Codigo Expositor',
            'dniExpositor' => 'Dni Expositor',
            'nombreExpositor' => 'Nombre Expositor',
            'nacionalidad' => 'Nacionalidad',
            'genero' => 'Genero',
            'correo' => 'Correo',
            'fechaNacimiento' => 'Fecha Nacimiento',
            'telefono' => 'Telefono',
        ];
    }

    /**
     * Gets query for [[Exponenconferencias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getExponenconferencias()
    {
        return $this->hasMany(Exponenconferencias::class, ['idExpositor' => 'codigoExpositor']);
    }

    /**
     * Gets query for [[IdConferencias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdConferencias()
    {
        return $this->hasMany(Conferencias::class, ['codigoConferencia' => 'idConferencia'])->viaTable('exponenconferencias', ['idExpositor' => 'codigoExpositor']);
    }
}
