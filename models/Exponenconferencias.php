<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "exponenconferencias".
 *
 * @property int $id
 * @property string|null $idConferencia
 * @property string|null $idExpositor
 *
 * @property Conferencias $idConferencia0
 * @property Expositores $idExpositor0
 */
class Exponenconferencias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'exponenconferencias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idConferencia', 'idExpositor'], 'string', 'max' => 9],
            [['idConferencia', 'idExpositor'], 'unique', 'targetAttribute' => ['idConferencia', 'idExpositor']],
            [['idConferencia'], 'exist', 'skipOnError' => true, 'targetClass' => Conferencias::class, 'targetAttribute' => ['idConferencia' => 'codigoConferencia']],
            [['idExpositor'], 'exist', 'skipOnError' => true, 'targetClass' => Expositores::class, 'targetAttribute' => ['idExpositor' => 'codigoExpositor']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idConferencia' => 'Id Conferencia',
            'idExpositor' => 'Id Expositor',
        ];
    }

    /**
     * Gets query for [[IdConferencia0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdConferencia0()
    {
        return $this->hasOne(Conferencias::class, ['codigoConferencia' => 'idConferencia']);
    }

    /**
     * Gets query for [[IdExpositor0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdExpositor0()
    {
        return $this->hasOne(Expositores::class, ['codigoExpositor' => 'idExpositor']);
    }
}
