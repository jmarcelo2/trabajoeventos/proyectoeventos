<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "carreras".
 *
 * @property string $codigoCarrera
 * @property string|null $nombreCarrera
 * @property string|null $facultad
 *
 * @property Conferencias[] $conferencias
 * @property Estudiantes[] $estudiantes
 * @property Talleres[] $talleres
 */
class Carreras extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'carreras';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigoCarrera'], 'required'],
            [['codigoCarrera'], 'string', 'max' => 9],
            [['nombreCarrera'], 'string', 'max' => 100],
            [['facultad'], 'string', 'max' => 150],
            [['codigoCarrera'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigoCarrera' => 'Codigo Carrera',
            'nombreCarrera' => 'Nombre Carrera',
            'facultad' => 'Facultad',
        ];
    }

    /**
     * Gets query for [[Conferencias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getConferencias()
    {
        return $this->hasMany(Conferencias::class, ['idCarrera' => 'codigoCarrera']);
    }

    /**
     * Gets query for [[Estudiantes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstudiantes()
    {
        return $this->hasMany(Estudiantes::class, ['idCarrera' => 'codigoCarrera']);
    }

    /**
     * Gets query for [[Talleres]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTalleres()
    {
        return $this->hasMany(Talleres::class, ['idCarrera' => 'codigoCarrera']);
    }
}
