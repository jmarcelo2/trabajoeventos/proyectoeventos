<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "correos".
 *
 * @property int $id
 * @property string|null $idEstudiante
 * @property string|null $correo
 *
 * @property Estudiantes $idEstudiante0
 */
class Correos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'correos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idEstudiante'], 'string', 'max' => 9],
            [['correo'], 'string', 'max' => 50],
            [['idEstudiante', 'correo'], 'unique', 'targetAttribute' => ['idEstudiante', 'correo']],
            [['idEstudiante'], 'exist', 'skipOnError' => true, 'targetClass' => Estudiantes::class, 'targetAttribute' => ['idEstudiante' => 'codigoEstudiante']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idEstudiante' => 'Id Estudiante',
            'correo' => 'Correo',
        ];
    }

    /**
     * Gets query for [[IdEstudiante0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstudiante0()
    {
        return $this->hasOne(Estudiantes::class, ['codigoEstudiante' => 'idEstudiante']);
    }
}
