<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "asistenconferencias".
 *
 * @property int $id
 * @property string|null $idEstudiante
 * @property string|null $idConferencia
 * @property string|null $horaEntrada
 * @property string|null $horaSalida
 *
 * @property Conferencias $idConferencia0
 * @property Estudiantes $idEstudiante0
 */
class Asistenconferencias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'asistenconferencias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['horaEntrada', 'horaSalida'], 'safe'],
            [['idEstudiante', 'idConferencia'], 'string', 'max' => 9],
            [['idConferencia', 'idEstudiante'], 'unique', 'targetAttribute' => ['idConferencia', 'idEstudiante']],
            [['idEstudiante'], 'exist', 'skipOnError' => true, 'targetClass' => Estudiantes::class, 'targetAttribute' => ['idEstudiante' => 'codigoEstudiante']],
            [['idConferencia'], 'exist', 'skipOnError' => true, 'targetClass' => Conferencias::class, 'targetAttribute' => ['idConferencia' => 'codigoConferencia']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idEstudiante' => 'Id Estudiante',
            'idConferencia' => 'Id Conferencia',
            'horaEntrada' => 'Hora Entrada',
            'horaSalida' => 'Hora Salida',
        ];
    }

    /**
     * Gets query for [[IdConferencia0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdConferencia0()
    {
        return $this->hasOne(Conferencias::class, ['codigoConferencia' => 'idConferencia']);
    }

    /**
     * Gets query for [[IdEstudiante0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstudiante0()
    {
        return $this->hasOne(Estudiantes::class, ['codigoEstudiante' => 'idEstudiante']);
    }
}
