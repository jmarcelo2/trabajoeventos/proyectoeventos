<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "conferencias".
 *
 * @property string $codigoConferencia
 * @property string|null $nombreConferencia
 * @property float|null $precio
 * @property string|null $fecha
 * @property string|null $horaEntrada
 * @property string|null $horaSalida
 * @property int|null $cupoMaximo
 * @property string|null $ubicación
 * @property string|null $idCarrera
 *
 * @property Asistenconferencias[] $asistenconferencias
 * @property Exponenconferencias[] $exponenconferencias
 * @property Carreras $idCarrera0
 * @property Estudiantes[] $idEstudiantes
 * @property Expositores[] $idExpositors
 * @property Temas[] $idTemas
 * @property Impartenconferencias[] $impartenconferencias
 */
class Conferencias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'conferencias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigoConferencia'], 'required'],
            [['precio'], 'number'],
            [['fecha', 'horaEntrada', 'horaSalida'], 'safe'],
            [['cupoMaximo'], 'integer'],
            [['codigoConferencia', 'idCarrera'], 'string', 'max' => 9],
            [['nombreConferencia'], 'string', 'max' => 100],
            [['ubicación'], 'string', 'max' => 50],
            [['codigoConferencia'], 'unique'],
            [['idCarrera'], 'exist', 'skipOnError' => true, 'targetClass' => Carreras::class, 'targetAttribute' => ['idCarrera' => 'codigoCarrera']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigoConferencia' => 'Codigo Conferencia',
            'nombreConferencia' => 'Nombre Conferencia',
            'precio' => 'Precio',
            'fecha' => 'Fecha',
            'horaEntrada' => 'Hora Entrada',
            'horaSalida' => 'Hora Salida',
            'cupoMaximo' => 'Cupo Maximo',
            'ubicación' => 'Ubicación',
            'idCarrera' => 'Id Carrera',
        ];
    }

    /**
     * Gets query for [[Asistenconferencias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAsistenconferencias()
    {
        return $this->hasMany(Asistenconferencias::class, ['idConferencia' => 'codigoConferencia']);
    }

    /**
     * Gets query for [[Exponenconferencias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getExponenconferencias()
    {
        return $this->hasMany(Exponenconferencias::class, ['idConferencia' => 'codigoConferencia']);
    }

    /**
     * Gets query for [[IdCarrera0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdCarrera0()
    {
        return $this->hasOne(Carreras::class, ['codigoCarrera' => 'idCarrera']);
    }

    /**
     * Gets query for [[IdEstudiantes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstudiantes()
    {
        return $this->hasMany(Estudiantes::class, ['codigoEstudiante' => 'idEstudiante'])->viaTable('asistenconferencias', ['idConferencia' => 'codigoConferencia']);
    }

    /**
     * Gets query for [[IdExpositors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdExpositors()
    {
        return $this->hasMany(Expositores::class, ['codigoExpositor' => 'idExpositor'])->viaTable('exponenconferencias', ['idConferencia' => 'codigoConferencia']);
    }

    /**
     * Gets query for [[IdTemas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdTemas()
    {
        return $this->hasMany(Temas::class, ['codigoTema' => 'idTema'])->viaTable('impartenconferencias', ['idConferencia' => 'codigoConferencia']);
    }

    /**
     * Gets query for [[Impartenconferencias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getImpartenconferencias()
    {
        return $this->hasMany(Impartenconferencias::class, ['idConferencia' => 'codigoConferencia']);
    }
}
