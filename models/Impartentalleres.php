<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "impartentalleres".
 *
 * @property int $id
 * @property string|null $idProfesor
 * @property string|null $idTaller
 *
 * @property Profesores $idProfesor0
 * @property Talleres $idTaller0
 */
class Impartentalleres extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'impartentalleres';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idProfesor', 'idTaller'], 'string', 'max' => 9],
            [['idProfesor', 'idTaller'], 'unique', 'targetAttribute' => ['idProfesor', 'idTaller']],
            [['idProfesor'], 'exist', 'skipOnError' => true, 'targetClass' => Profesores::class, 'targetAttribute' => ['idProfesor' => 'codigoProfesor']],
            [['idTaller'], 'exist', 'skipOnError' => true, 'targetClass' => Talleres::class, 'targetAttribute' => ['idTaller' => 'codigoTaller']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idProfesor' => 'Id Profesor',
            'idTaller' => 'Id Taller',
        ];
    }

    /**
     * Gets query for [[IdProfesor0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdProfesor0()
    {
        return $this->hasOne(Profesores::class, ['codigoProfesor' => 'idProfesor']);
    }

    /**
     * Gets query for [[IdTaller0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdTaller0()
    {
        return $this->hasOne(Talleres::class, ['codigoTaller' => 'idTaller']);
    }
}
