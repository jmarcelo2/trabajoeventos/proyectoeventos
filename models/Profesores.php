<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "profesores".
 *
 * @property string $codigoProfesor
 * @property string|null $dniProfesor
 * @property string|null $nombreCompleto
 * @property string|null $direccion
 * @property string|null $telefono
 * @property string|null $nivelEducación
 * @property string|null $fechaNacimiento
 * @property string|null $correo
 *
 * @property Talleres[] $idTallers
 * @property Impartentalleres[] $impartentalleres
 */
class Profesores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'profesores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigoProfesor'], 'required'],
            [['fechaNacimiento'], 'safe'],
            [['codigoProfesor', 'dniProfesor', 'telefono'], 'string', 'max' => 9],
            [['nombreCompleto', 'direccion', 'correo'], 'string', 'max' => 50],
            [['nivelEducación'], 'string', 'max' => 25],
            [['dniProfesor'], 'unique'],
            [['codigoProfesor'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigoProfesor' => 'Codigo Profesor',
            'dniProfesor' => 'Dni Profesor',
            'nombreCompleto' => 'Nombre Completo',
            'direccion' => 'Direccion',
            'telefono' => 'Telefono',
            'nivelEducación' => 'Nivel Educación',
            'fechaNacimiento' => 'Fecha Nacimiento',
            'correo' => 'Correo',
        ];
    }

    /**
     * Gets query for [[IdTallers]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdTallers()
    {
        return $this->hasMany(Talleres::class, ['codigoTaller' => 'idTaller'])->viaTable('impartentalleres', ['idProfesor' => 'codigoProfesor']);
    }

    /**
     * Gets query for [[Impartentalleres]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getImpartentalleres()
    {
        return $this->hasMany(Impartentalleres::class, ['idProfesor' => 'codigoProfesor']);
    }
}
