<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "asistentalleres".
 *
 * @property int $id
 * @property string|null $idEstudiante
 * @property string|null $idTaller
 * @property string|null $horaEntrada
 * @property string|null $horaSalida
 *
 * @property Estudiantes $idEstudiante0
 * @property Talleres $idTaller0
 */
class Asistentalleres extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'asistentalleres';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['horaEntrada', 'horaSalida'], 'safe'],
            [['idEstudiante', 'idTaller'], 'string', 'max' => 9],
            [['idTaller', 'idEstudiante'], 'unique', 'targetAttribute' => ['idTaller', 'idEstudiante']],
            [['idEstudiante'], 'exist', 'skipOnError' => true, 'targetClass' => Estudiantes::class, 'targetAttribute' => ['idEstudiante' => 'codigoEstudiante']],
            [['idTaller'], 'exist', 'skipOnError' => true, 'targetClass' => Talleres::class, 'targetAttribute' => ['idTaller' => 'codigoTaller']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idEstudiante' => 'Id Estudiante',
            'idTaller' => 'Id Taller',
            'horaEntrada' => 'Hora Entrada',
            'horaSalida' => 'Hora Salida',
        ];
    }

    /**
     * Gets query for [[IdEstudiante0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstudiante0()
    {
        return $this->hasOne(Estudiantes::class, ['codigoEstudiante' => 'idEstudiante']);
    }

    /**
     * Gets query for [[IdTaller0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdTaller0()
    {
        return $this->hasOne(Talleres::class, ['codigoTaller' => 'idTaller']);
    }
}
