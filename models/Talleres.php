<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "talleres".
 *
 * @property string $codigoTaller
 * @property string|null $nombreTaller
 * @property string|null $fecha
 * @property string|null $horaEntrada
 * @property string|null $horaSalida
 * @property float|null $precio
 * @property string|null $ubicacion
 * @property int|null $cupoMaximo
 * @property string|null $idCarrera
 *
 * @property Asistentalleres[] $asistentalleres
 * @property Carreras $idCarrera0
 * @property Estudiantes[] $idEstudiantes
 * @property Profesores[] $idProfesors
 * @property Impartentalleres[] $impartentalleres
 */
class Talleres extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'talleres';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigoTaller'], 'required'],
            [['fecha', 'horaEntrada', 'horaSalida'], 'safe'],
            [['precio'], 'number'],
            [['ubicacion'], 'string'],
            [['cupoMaximo'], 'integer'],
            [['codigoTaller', 'idCarrera'], 'string', 'max' => 9],
            [['nombreTaller'], 'string', 'max' => 255],
            [['codigoTaller'], 'unique'],
            [['idCarrera'], 'exist', 'skipOnError' => true, 'targetClass' => Carreras::class, 'targetAttribute' => ['idCarrera' => 'codigoCarrera']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigoTaller' => 'Codigo Taller',
            'nombreTaller' => 'Nombre Taller',
            'fecha' => 'Fecha',
            'horaEntrada' => 'Hora Entrada',
            'horaSalida' => 'Hora Salida',
            'precio' => 'Precio',
            'ubicacion' => 'Ubicacion',
            'cupoMaximo' => 'Cupo Maximo',
            'idCarrera' => 'Id Carrera',
        ];
    }

    /**
     * Gets query for [[Asistentalleres]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAsistentalleres()
    {
        return $this->hasMany(Asistentalleres::class, ['idTaller' => 'codigoTaller']);
    }

    /**
     * Gets query for [[IdCarrera0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdCarrera0()
    {
        return $this->hasOne(Carreras::class, ['codigoCarrera' => 'idCarrera']);
    }

    /**
     * Gets query for [[IdEstudiantes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstudiantes()
    {
        return $this->hasMany(Estudiantes::class, ['codigoEstudiante' => 'idEstudiante'])->viaTable('asistentalleres', ['idTaller' => 'codigoTaller']);
    }

    /**
     * Gets query for [[IdProfesors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdProfesors()
    {
        return $this->hasMany(Profesores::class, ['codigoProfesor' => 'idProfesor'])->viaTable('impartentalleres', ['idTaller' => 'codigoTaller']);
    }

    /**
     * Gets query for [[Impartentalleres]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getImpartentalleres()
    {
        return $this->hasMany(Impartentalleres::class, ['idTaller' => 'codigoTaller']);
    }
}
