<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "temas".
 *
 * @property string $codigoTema
 * @property string|null $nombreTema
 *
 * @property Conferencias[] $idConferencias
 * @property Impartenconferencias[] $impartenconferencias
 */
class Temas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'temas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigoTema'], 'required'],
            [['codigoTema'], 'string', 'max' => 9],
            [['nombreTema'], 'string', 'max' => 100],
            [['codigoTema'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigoTema' => 'Codigo Tema',
            'nombreTema' => 'Nombre Tema',
        ];
    }

    /**
     * Gets query for [[IdConferencias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdConferencias()
    {
        return $this->hasMany(Conferencias::class, ['codigoConferencia' => 'idConferencia'])->viaTable('impartenconferencias', ['idTema' => 'codigoTema']);
    }

    /**
     * Gets query for [[Impartenconferencias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getImpartenconferencias()
    {
        return $this->hasMany(Impartenconferencias::class, ['idTema' => 'codigoTema']);
    }
}
