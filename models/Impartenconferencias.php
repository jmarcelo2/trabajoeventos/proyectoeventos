<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "impartenconferencias".
 *
 * @property int $id
 * @property string|null $idConferencia
 * @property string|null $idTema
 *
 * @property Conferencias $idConferencia0
 * @property Temas $idTema0
 */
class Impartenconferencias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'impartenconferencias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idConferencia', 'idTema'], 'string', 'max' => 9],
            [['idTema', 'idConferencia'], 'unique', 'targetAttribute' => ['idTema', 'idConferencia']],
            [['idTema'], 'exist', 'skipOnError' => true, 'targetClass' => Temas::class, 'targetAttribute' => ['idTema' => 'codigoTema']],
            [['idConferencia'], 'exist', 'skipOnError' => true, 'targetClass' => Conferencias::class, 'targetAttribute' => ['idConferencia' => 'codigoConferencia']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idConferencia' => 'Id Conferencia',
            'idTema' => 'Id Tema',
        ];
    }

    /**
     * Gets query for [[IdConferencia0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdConferencia0()
    {
        return $this->hasOne(Conferencias::class, ['codigoConferencia' => 'idConferencia']);
    }

    /**
     * Gets query for [[IdTema0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdTema0()
    {
        return $this->hasOne(Temas::class, ['codigoTema' => 'idTema']);
    }
}
