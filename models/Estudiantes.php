<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estudiantes".
 *
 * @property string $codigoEstudiante
 * @property string|null $nombreCompleto
 * @property string|null $dniEstudiante
 * @property string|null $direccion
 * @property string|null $telefono
 * @property string|null $genero
 * @property string|null $fechaNacimiento
 * @property string|null $idCarrera
 *
 * @property Asistenconferencias[] $asistenconferencias
 * @property Asistentalleres[] $asistentalleres
 * @property Correos[] $correos
 * @property Carreras $idCarrera0
 * @property Conferencias[] $idConferencias
 * @property Talleres[] $idTallers
 */
class Estudiantes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'estudiantes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigoEstudiante'], 'required'],
            [['fechaNacimiento'], 'safe'],
            [['codigoEstudiante', 'dniEstudiante', 'telefono', 'idCarrera'], 'string', 'max' => 9],
            [['nombreCompleto', 'direccion'], 'string', 'max' => 50],
            [['genero'], 'string', 'max' => 10],
            [['dniEstudiante'], 'unique'],
            [['codigoEstudiante'], 'unique'],
            [['idCarrera'], 'exist', 'skipOnError' => true, 'targetClass' => Carreras::class, 'targetAttribute' => ['idCarrera' => 'codigoCarrera']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigoEstudiante' => 'Codigo Estudiante',
            'nombreCompleto' => 'Nombre Completo',
            'dniEstudiante' => 'Dni Estudiante',
            'direccion' => 'Direccion',
            'telefono' => 'Telefono',
            'genero' => 'Genero',
            'fechaNacimiento' => 'Fecha Nacimiento',
            'idCarrera' => 'Id Carrera',
        ];
    }

    /**
     * Gets query for [[Asistenconferencias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAsistenconferencias()
    {
        return $this->hasMany(Asistenconferencias::class, ['idEstudiante' => 'codigoEstudiante']);
    }

    /**
     * Gets query for [[Asistentalleres]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAsistentalleres()
    {
        return $this->hasMany(Asistentalleres::class, ['idEstudiante' => 'codigoEstudiante']);
    }

    /**
     * Gets query for [[Correos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCorreos()
    {
        return $this->hasMany(Correos::class, ['idEstudiante' => 'codigoEstudiante']);
    }

    /**
     * Gets query for [[IdCarrera0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdCarrera0()
    {
        return $this->hasOne(Carreras::class, ['codigoCarrera' => 'idCarrera']);
    }

    /**
     * Gets query for [[IdConferencias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdConferencias()
    {
        return $this->hasMany(Conferencias::class, ['codigoConferencia' => 'idConferencia'])->viaTable('asistenconferencias', ['idEstudiante' => 'codigoEstudiante']);
    }

    /**
     * Gets query for [[IdTallers]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdTallers()
    {
        return $this->hasMany(Talleres::class, ['codigoTaller' => 'idTaller'])->viaTable('asistentalleres', ['idEstudiante' => 'codigoEstudiante']);
    }
}
