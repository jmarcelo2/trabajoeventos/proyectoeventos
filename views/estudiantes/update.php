<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Estudiantes $model */

$this->title = 'Update Estudiantes: ' . $model->codigoEstudiante;
$this->params['breadcrumbs'][] = ['label' => 'Estudiantes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigoEstudiante, 'url' => ['view', 'codigoEstudiante' => $model->codigoEstudiante]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="estudiantes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
