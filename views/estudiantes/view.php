<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Estudiantes $model */

$this->title = $model->codigoEstudiante;
$this->params['breadcrumbs'][] = ['label' => 'Estudiantes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="estudiantes-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'codigoEstudiante' => $model->codigoEstudiante], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'codigoEstudiante' => $model->codigoEstudiante], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigoEstudiante',
            'nombreCompleto',
            'dniEstudiante',
            'direccion',
            'telefono',
            'genero',
            'fechaNacimiento',
            'idCarrera',
        ],
    ]) ?>

</div>
