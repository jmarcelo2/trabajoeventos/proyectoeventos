<?php

use app\models\Estudiantes;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Estudiantes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estudiantes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Estudiantes', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigoEstudiante',
            'nombreCompleto',
            'dniEstudiante',
            'direccion',
            'telefono',
            //'genero',
            //'fechaNacimiento',
            //'idCarrera',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Estudiantes $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codigoEstudiante' => $model->codigoEstudiante]);
                 }
            ],
        ],
    ]); ?>


</div>
