<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ImpartenTalleres $model */

$this->title = 'Update Imparten Talleres: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Imparten Talleres', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="imparten-talleres-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
