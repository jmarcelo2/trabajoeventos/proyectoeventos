<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\ImpartenTalleres $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="imparten-talleres-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idProfesor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'idTaller')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
