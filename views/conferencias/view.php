<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Conferencias $model */

$this->title = $model->codigoConferencia;
$this->params['breadcrumbs'][] = ['label' => 'Conferencias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="conferencias-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'codigoConferencia' => $model->codigoConferencia], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'codigoConferencia' => $model->codigoConferencia], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigoConferencia',
            'nombreConferencia',
            'precio',
            'fecha',
            'horaEntrada',
            'horaSalida',
            'cupoMaximo',
            'ubicación',
            'idCarrera',
        ],
    ]) ?>

</div>
