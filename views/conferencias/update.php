<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Conferencias $model */

$this->title = 'Update Conferencias: ' . $model->codigoConferencia;
$this->params['breadcrumbs'][] = ['label' => 'Conferencias', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigoConferencia, 'url' => ['view', 'codigoConferencia' => $model->codigoConferencia]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="conferencias-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
