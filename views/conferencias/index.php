<?php

use app\models\Conferencias;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Conferencias';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="conferencias-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Conferencias', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigoConferencia',
            'nombreConferencia',
            'precio',
            'fecha',
            'horaEntrada',
            //'horaSalida',
            //'cupoMaximo',
            //'ubicación',
            //'idCarrera',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Conferencias $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codigoConferencia' => $model->codigoConferencia]);
                 }
            ],
        ],
    ]); ?>


</div>
