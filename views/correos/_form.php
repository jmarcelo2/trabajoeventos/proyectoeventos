<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Correos $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="correos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idEstudiante')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'correo')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
