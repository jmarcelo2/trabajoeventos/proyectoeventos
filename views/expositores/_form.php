<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Expositores $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="expositores-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigoExpositor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dniExpositor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombreExpositor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nacionalidad')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'genero')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'correo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fechaNacimiento')->textInput() ?>

    <?= $form->field($model, 'telefono')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
