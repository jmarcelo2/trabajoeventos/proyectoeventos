<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Expositores $model */

$this->title = 'Create Expositores';
$this->params['breadcrumbs'][] = ['label' => 'Expositores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="expositores-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
