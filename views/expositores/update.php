<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Expositores $model */

$this->title = 'Update Expositores: ' . $model->codigoExpositor;
$this->params['breadcrumbs'][] = ['label' => 'Expositores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigoExpositor, 'url' => ['view', 'codigoExpositor' => $model->codigoExpositor]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="expositores-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
