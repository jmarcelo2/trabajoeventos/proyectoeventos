<?php

use app\models\Expositores;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Expositores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="expositores-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Expositores', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigoExpositor',
            'dniExpositor',
            'nombreExpositor',
            'nacionalidad',
            'genero',
            //'correo',
            //'fechaNacimiento',
            //'telefono',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Expositores $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codigoExpositor' => $model->codigoExpositor]);
                 }
            ],
        ],
    ]); ?>


</div>
