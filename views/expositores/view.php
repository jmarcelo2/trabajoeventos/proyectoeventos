<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Expositores $model */

$this->title = $model->codigoExpositor;
$this->params['breadcrumbs'][] = ['label' => 'Expositores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="expositores-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'codigoExpositor' => $model->codigoExpositor], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'codigoExpositor' => $model->codigoExpositor], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigoExpositor',
            'dniExpositor',
            'nombreExpositor',
            'nacionalidad',
            'genero',
            'correo',
            'fechaNacimiento',
            'telefono',
        ],
    ]) ?>

</div>
