<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Carreras $model */

$this->title = 'Update Carreras: ' . $model->codigoCarrera;
$this->params['breadcrumbs'][] = ['label' => 'Carreras', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigoCarrera, 'url' => ['view', 'codigoCarrera' => $model->codigoCarrera]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="carreras-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
