<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Carreras $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="carreras-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigoCarrera')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombreCarrera')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'facultad')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
