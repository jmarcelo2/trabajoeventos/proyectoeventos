<?php

use app\models\Carreras;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Carreras';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="carreras-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Carreras', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigoCarrera',
            'nombreCarrera',
            'facultad',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Carreras $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codigoCarrera' => $model->codigoCarrera]);
                 }
            ],
        ],
    ]); ?>


</div>
