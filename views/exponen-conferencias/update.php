<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ExponenConferencias $model */

$this->title = 'Update Exponen Conferencias: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Exponen Conferencias', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="exponen-conferencias-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
