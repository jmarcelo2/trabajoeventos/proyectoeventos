<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\ExponenConferencias $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="exponen-conferencias-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idConferencia')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'idExpositor')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
