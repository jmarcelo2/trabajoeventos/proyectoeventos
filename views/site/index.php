<?php

//use yii\helpers\Html
use yii\helpers\Url;
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Image Slider</title>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Dosis&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">

        <link rel="stylesheet" href="<?= Url::to('@web/css/styles.css') ?>">

    </head>
    <body>

        <div id="slider-container">
            <div id="slider">
                <div class="slide">
                    <img  src="<?= Yii::getAlias('@web') ?>../images/evento1.jpg" alt="Slide 1">
                    <div class="text-container">Primer Evento tecnológico de Big Data</div>

                </div>
                <div class="slide">
                    <img  src="<?= Yii::getAlias('@web') ?>../images/evento2.jpeg" alt="Slide 2">
                    <div class="text-container">Segundo Evento Tecnológico de Inteligencia de Negocios</div>
                </div>
                <div class="slide">
                    <img  src="<?= Yii::getAlias('@web') ?>../images/evento3.jpg" alt="Slide 3">
                    <div class="text-container">Tercer Evento tecnológico de POWER BI</div>
                </div>
                <!-- Add more slides as needed -->
            </div>

        </div>
        <main class="mx-auto my-4">
            <section class="contenedor3 mb-4">
                <div class="container">
                    <!-- Tarjeta Bonita (Bienvenida) -->
                    <div class="row mt-4">
                        <div class="col-md-12">
                            <div class="card bonita-card">
                                <div class="card-body">
                                    <h3 class="card-title">Bienvenido a <b>Smart Audience</b></h3>
                                    <p class="card-text">Este es un apartado donde te daremos información 
                                        relevante de todos los eventos que estaremos realizando de tu Universidad. Te daremos toda la información que necesitas, el horario, talleres, conferencias que 
                                        se van a realizar en la Universidad ya sea de cualquier carrera,
                                        tendrás los horaios a tu disposición. </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Nuevas Cards -->
                    <div class="row mt-4">
                        <!-- Card Talleres -->
                        <div class="col-md-4 mb-4">
                            <div class="card small-card">
                                <i class="fas fa-chalkboard-teacher fa-3x mb-3"></i>
                                <div class="card-body">
                                    <a href="../talleres/index">Talleres</a>
                                </div>
                            </div>
                        </div>

                        <!-- Card Congresos -->
                        <div class="col-md-4 mb-4">
                            <div class="card small-card">
                                <i class="fas fa-calendar-alt fa-3x mb-3"></i>
                                <div class="card-body">
                                    <a href="../conferencias/index">Congresos</a>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-4 mb-4">
                            <div class="card small-card">
                                <i class="fas fa-chalkboard-teacher fa-3x mb-3"></i>
                                <div class="card-body">
                                    <a href="../profesores/index">Profesores</a>
                                </div>
                            </div>
                        </div>

                        <!-- Card Temas -->
                        <div class="col-md-4 mb-4">
                            <div class="card small-card">
                                <i class="fas fa-book fa-3x mb-3"></i>
                                <div class="card-body">
                                    <a href="../temas/index">Temas</a>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-4 mb-4">
                            <div class="card small-card">
                                <i class="fas fa-user-graduate fa-3x mb-3"></i>
                                <div class="card-body">
                                    <a href="../estudiantes/index">Alumnos</a>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-4 mb-4">
                            <div class="card small-card">
                                <i class="fas fa-university fa-3x mb-3"></i>
                                <div class="card-body">
                                    <a href="../carreras/index">Carreras</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


        </section>
        <section class="contenedor2">
            <h1 class="texto-medio">Testimonios</h1>
            <div class="testimonial-container">
                <div id="testimonialCarousel" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="testimonial-slide text-center mx-auto" style="max-width: 500px;">
                                <div class="testimonial-image-container rounded-circle overflow-hidden" style="width: 150px; height: 100px;object-fit: cover">
                                    <img src="<?= Yii::getAlias('@web') ?>../images/imagen1.jpg" class="d-block w-100 h-100" alt="Testimonial 1">
                                </div>
                                <div class="testimonial-content mt-3">
                                    <p>Gracias a esta página web me pude enterar de los horarios de los eventos que desarrollarán en mi Facultad.</p>
                                    <p>Camila Echevarría</p>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="testimonial-slide text-center mx-auto" style="max-width: 500px;">
                                <div class="testimonial-image-container rounded-circle overflow-hidden" style="width: 150px; height: 100px;object-fit: cover">
                                    <img src="<?= Yii::getAlias('@web') ?>../images/imagen2.jpg" class="d-block w-100 h-100" alt="Testimonial 2">
                                </div>
                                <div class="testimonial-content mt-3">
                                    <p>Gracias a esta página web me pude enterar de los horarios de los eventos que desarrollarán en mi Facultad.</p>
                                    <p>Claudia Aramburu</p>
                                </div>
                            </div>
                        </div>

                    </div>
                    <a class="carousel-control-prev" href="#testimonialCarousel" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#testimonialCarousel" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </section>

        <h1 class="texto-medio">Últimos eventos</h1>
        <section class="contenedor text-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 mb-4">
                        <div class="card">
                            <img src="<?= Yii::getAlias('@web') ?>../images/academia1.jpg" class="card-img-top img-fluid" alt="Card 1">
                            <div class="card-body">
                                <h5 class="card-title">Evento de Economía</h5>
                                <p class="card-text">Este evento fue desarrollado en el Auditorio Principal de la universidad, con un total de 400 espectadores, se llevó con éxito el evento.</p>
                                <a href="#" class="stretched-link">Leer más</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 mb-4">
                        <div class="card">
                            <img src="<?= Yii::getAlias('@web') ?>../images/academia2.jpg" class="card-img-top img-fluid" alt="Card 2">
                            <div class="card-body">
                                <h5 class="card-title">Evento de Ing. Industrial</h5>
                                <p class="card-text">Este evento fue desarrollado en el Auditorio Principal de la universidad, con un total de 400 espectadores, se llevó con éxito el evento.</p>
                                <a href="#" class="stretched-link">Leer más</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 mb-4">
                        <div class="card">
                            <img src="<?= Yii::getAlias('@web') ?>../images/academia3.jpg" class="card-img-top img-fluid" alt="Card 3">
                            <div class="card-body">
                                <h5 class="card-title">Evento de Marketing Digital</h5>
                                <p class="card-text">Este evento fue desarrollado en el Auditorio Principal de la universidad, con un total de 400 espectadores, se llevó con éxito el evento.</p>
                                <a href="#" class="stretched-link">Leer más</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="contenedor4 text-center">
            <h1 class="texto-medio">Eventos realizados</h1>
            <div class="container">
                <div class="row">
                    <div class="col-md-3 mb-4">
                        <div class="loading-ball uno" id="exhibitorsCounter">
                            <div class="loading-label">
                                <i class="fas fa-users"></i>
                                <div id="participantsNumber">0</div>
                                <div>Participantes</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 mb-4">
                        <div class="loading-ball dos" id="participantsCounter">
                            <div class="loading-label">
                                <i class="fas fa-chalkboard-teacher"></i>
                                <div id="expositoresNumber">0</div>
                                <div>Expositores</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 mb-4">
                        <div class="loading-ball tres" id="talleresCounter">
                            <div class="loading-label">
                                <i class="fas fa-calendar-alt"></i>
                                <div id="talleresNumber">0</div>
                                <div>Talleres</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 mb-4">
                        <div class="loading-ball cuatro" id="congresosCounter">
                            <div class="loading-label">
                                <i class="fas fa-calendar-alt"></i>
                                <div id="congresosNumber">0</div>
                                <div>Congresos</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="gallery text-center">
            <h1 class="texto-medio">Galería de eventos</h1>
            <div class="container">
                <div class="row">
                    <?php for ($i = 1; $i <= 8; $i++) : ?>
                        <div class="col-md-3 my-auto">
                            <a href="#" data-toggle="modal" data-target="#myModal<?= $i ?>">
                                <img src="<?= Yii::getAlias('@web') ?>../images/evento-<?= $i ?>.jpg" class="img-fluid" alt="Imagen <?= $i ?>">
                            </a>
                        </div>
                    <?php endfor; ?>
                </div>
            </div>
        </section>

    </main>

    <?php for ($i = 1; $i <= 8; $i++) : ?>
        <div class="modal fade" id="myModal<?= $i ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel<?= $i ?>">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="myModalLabel<?= $i ?>">Título del Modal <?= $i ?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <!-- Slider de Fotos Similares para Modal <?= $i ?> -->
                        <div id="carouselModal<?= $i ?>" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img src="<?= Yii::getAlias('@web') ?>../images/evento_<?= $i ?>_1.jpg" class="d-block w-100" alt="Foto Similar <?= $i ?> 1">
                                </div>
                                <div class="carousel-item">
                                    <img  src="<?= Yii::getAlias('@web') ?>../images/evento_<?= $i ?>_2.jpg" class="d-block w-100" alt="Foto Similar <?= $i ?> 2">
                                </div>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endfor; ?>

    <footer class="text-light py-5">
        <div class="container">
            <div class="row">
             
                <div class="col-md-3 d-flex justify-content-center align-items-center">
                    <img src="<?= Yii::getAlias('@web') ?>../images/logo2.png" alt="Logo de la Marca" class="img-fluid rounded-circle" style="width: 80px;">
                </div>
               
                <div class="col-md-6 d-flex align-items-center justify-content-center">
                    <ul class="list-inline">
                        <li class="list-inline-item"><a href="#" target="_blank"><i class="fab fa-facebook-square fa-2x"></i></a></li>
                        <li class="list-inline-item"><a href="#" target="_blank"><i class="fab fa-twitter-square fa-2x"></i></a></li>
                        <li class="list-inline-item"><a href="#" target="_blank"><i class="fab fa-instagram-square fa-2x"></i></a></li>
                   
                    </ul>
                </div>
              
                <div class="col-md-3 text-md-right">
                    <div class="d-flex flex-column align-items-center">
                        <div class="mb-2">
                            <i class="fas fa-map-marker-alt"></i> Dirección: Calle Principal, Ciudad
                        </div>
                        <div>
                            <i class="fas fa-phone-alt"></i> Teléfono: (123) 456-7890
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer::after></footer::after>
        <div class="footer-text">
            Desarrollado con Yii2
        </div>
    </footer>

    <script>
        const slider = document.getElementById('slider');
        let currentIndex = 0;

        function nextSlide() {
            currentIndex = (currentIndex + 1) % slider.children.length;
            updateSlider();
        }

        function updateSlider() {
            const transformValue = `translateX(-${currentIndex * 100}%)`;
            slider.style.transform = transformValue;
        }

        setInterval(nextSlide, 3000); 

      

        let cont1 = document.getElementById('participantsNumber'),
                cont2 = document.getElementById('expositoresNumber'),
                cont3 = document.getElementById('talleresNumber'),
                cont4 = document.getElementById('congresosNumber');
        let cant1 = 0, cant2 = 0, cant3 = 0, cant4 = 0;
        tiem = 15;

        let tiempo1 = setInterval(() => {
            cont1.textContent = cant1 += 1;
            if (cant1 === 170) {
                clearInterval(tiempo1);
            }
        }, `${tiem}`);

        let tiempo2 = setInterval(() => {
            cont2.textContent = cant2 += 1;
            if (cant2 === 240) {
                clearInterval(tiempo2);
            }
        }, `${tiem}`);

        let tiempo3 = setInterval(() => {
            cont3.textContent = cant3 += 1;
            if (cant3 === 230) {
                clearInterval(tiempo3);
            }
        }, `${tiem}`);

        let tiempo4 = setInterval(() => {
            cont4.textContent = cant4 += 1;
            if (cant4 === 200) {
                clearInterval(tiempo4);
            }
        }, `${tiem}`);
    </script>
</body>
</html>