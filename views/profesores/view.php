<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Profesores $model */

$this->title = $model->codigoProfesor;
$this->params['breadcrumbs'][] = ['label' => 'Profesores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="profesores-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'codigoProfesor' => $model->codigoProfesor], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'codigoProfesor' => $model->codigoProfesor], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigoProfesor',
            'dniProfesor',
            'nombreCompleto',
            'direccion',
            'telefono',
            'nivelEducación',
            'fechaNacimiento',
            'correo',
        ],
    ]) ?>

</div>
