<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Profesores $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="profesores-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigoProfesor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dniProfesor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombreCompleto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'direccion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'telefono')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nivelEducación')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fechaNacimiento')->textInput() ?>

    <?= $form->field($model, 'correo')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
