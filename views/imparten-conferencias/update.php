<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ImpartenConferencias $model */

$this->title = 'Update Imparten Conferencias: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Imparten Conferencias', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="imparten-conferencias-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
