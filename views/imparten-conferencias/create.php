<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ImpartenConferencias $model */

$this->title = 'Create Imparten Conferencias';
$this->params['breadcrumbs'][] = ['label' => 'Imparten Conferencias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="imparten-conferencias-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
