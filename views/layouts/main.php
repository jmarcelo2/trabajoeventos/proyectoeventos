<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
     <link rel="stylesheet" href="<?=Url::to('@web/css/main.css')?>">
  
</head>
<body class="d-flex flex-column h-100 "  style="width:100%">
<?php $this->beginBody() ?>

<header>
    <?php
    NavBar::begin([
        'brandLabel' => '<img src="' . Yii::getAlias('@web') . '/images/logo3.png" alt="Logo" class="img-fluid" style="width:50px; height:50px">',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-expand-md navbar-dark bg-dark fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'About', 'url' => ['/site/about']],
            ['label' => 'Contact', 'url' => ['/site/contact']],
                [
                        'label' => 'Administración',
                        'items' => [
                            ['label' => 'Profesores', 'url' => ['/profesores/index']],
                            ['label' => 'Talleres', 'url' => ['/talleres/index']],
                            ['label' => 'Conferencias', 'url' => ['/conferencias/index']],
                            ['label' => 'Carreras', 'url' => ['/carreras/index']],
                            ['label' => 'Correos', 'url' => ['/correos/index']],
                            ['label' => 'ImpartenTalleres', 'url' => ['/imparten-talleres/index']],
                            ['label' => 'Carreras', 'url' => ['/carreras/index']],
                            ['label' => 'ImpartenConferencias', 'url' => ['/imparten-conferencias/index']],
                            ['label' => 'Expositores', 'url' => ['/expositores/index']],
                            ['label' => 'AsistenTalleres', 'url' => ['/asisten-talleres/index']],
                            ['label' => 'AsistenConferencias', 'url' => ['/asisten-conferencias/index']],
                            ['label' => 'Temas', 'url' => ['/temas/index']],
                            ['label' => 'ExponenConferencias', 'url' => ['/exponen-conferencias/index']],
                        ],
                    ],
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post', ['class' => 'form-inline'])
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>
</header>

    <main role="main"  class="flex-shrink-0">
    <div class="container" >
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
    </main>



<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
