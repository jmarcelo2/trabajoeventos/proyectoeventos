<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\AsistenConferencias $model */

$this->title = 'Create Asisten Conferencias';
$this->params['breadcrumbs'][] = ['label' => 'Asisten Conferencias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asisten-conferencias-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
