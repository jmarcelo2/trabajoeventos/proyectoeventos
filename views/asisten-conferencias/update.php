<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\AsistenConferencias $model */

$this->title = 'Update Asisten Conferencias: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Asisten Conferencias', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="asisten-conferencias-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
