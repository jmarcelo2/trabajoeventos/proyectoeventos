<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\AsistenConferencias $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="asisten-conferencias-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idEstudiante')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'idConferencia')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'horaEntrada')->textInput() ?>

    <?= $form->field($model, 'horaSalida')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
