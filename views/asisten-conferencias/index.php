<?php

use app\models\AsistenConferencias;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Asisten Conferencias';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asisten-conferencias-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Asisten Conferencias', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'idEstudiante',
            'idConferencia',
            'horaEntrada',
            'horaSalida',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, AsistenConferencias $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
