<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Talleres $model */

$this->title = $model->codigoTaller;
$this->params['breadcrumbs'][] = ['label' => 'Talleres', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="talleres-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'codigoTaller' => $model->codigoTaller], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'codigoTaller' => $model->codigoTaller], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigoTaller',
            'nombreTaller',
            'fecha',
            'horaEntrada',
            'horaSalida',
            'precio',
            'ubicacion:ntext',
            'cupoMaximo',
            'idCarrera',
        ],
    ]) ?>

</div>
