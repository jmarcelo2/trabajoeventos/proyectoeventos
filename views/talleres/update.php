<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Talleres $model */

$this->title = 'Update Talleres: ' . $model->codigoTaller;
$this->params['breadcrumbs'][] = ['label' => 'Talleres', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigoTaller, 'url' => ['view', 'codigoTaller' => $model->codigoTaller]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="talleres-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
