<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Talleres $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="talleres-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigoTaller')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombreTaller')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha')->textInput() ?>

    <?= $form->field($model, 'horaEntrada')->textInput() ?>

    <?= $form->field($model, 'horaSalida')->textInput() ?>

    <?= $form->field($model, 'precio')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ubicacion')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'cupoMaximo')->textInput() ?>

    <?= $form->field($model, 'idCarrera')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
