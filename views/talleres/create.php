<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Talleres $model */

$this->title = 'Create Talleres';
$this->params['breadcrumbs'][] = ['label' => 'Talleres', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="talleres-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
