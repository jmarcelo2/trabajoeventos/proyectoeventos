<?php

use app\models\Talleres;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Talleres';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="talleres-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Talleres', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigoTaller',
            'nombreTaller',
            'fecha',
            'horaEntrada',
            'horaSalida',
            //'precio',
            //'ubicacion:ntext',
            //'cupoMaximo',
            //'idCarrera',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Talleres $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codigoTaller' => $model->codigoTaller]);
                 }
            ],
        ],
    ]); ?>


</div>
