<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\AsistenTalleres $model */

$this->title = 'Update Asisten Talleres: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Asisten Talleres', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="asisten-talleres-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
