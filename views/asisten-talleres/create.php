<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\AsistenTalleres $model */

$this->title = 'Create Asisten Talleres';
$this->params['breadcrumbs'][] = ['label' => 'Asisten Talleres', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asisten-talleres-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
