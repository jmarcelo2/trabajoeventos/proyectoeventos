<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\AsistenTalleres $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="asisten-talleres-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idEstudiante')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'idTaller')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'horaEntrada')->textInput() ?>

    <?= $form->field($model, 'horaSalida')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
