<?php

use app\models\AsistenTalleres;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Asisten Talleres';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asisten-talleres-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Asisten Talleres', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'idEstudiante',
            'idTaller',
            'horaEntrada',
            'horaSalida',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, AsistenTalleres $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
