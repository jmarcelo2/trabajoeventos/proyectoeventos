<?php

use app\models\Temas;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Temas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="temas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Temas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigoTema',
            'nombreTema',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Temas $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codigoTema' => $model->codigoTema]);
                 }
            ],
        ],
    ]); ?>


</div>
