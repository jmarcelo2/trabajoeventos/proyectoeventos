<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Temas $model */

$this->title = 'Update Temas: ' . $model->codigoTema;
$this->params['breadcrumbs'][] = ['label' => 'Temas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigoTema, 'url' => ['view', 'codigoTema' => $model->codigoTema]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="temas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
