<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Temas $model */

$this->title = 'Create Temas';
$this->params['breadcrumbs'][] = ['label' => 'Temas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="temas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
