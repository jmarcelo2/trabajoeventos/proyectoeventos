﻿DROP DATABASE IF EXISTS eventosuniversidad;
CREATE DATABASE eventosuniversidad;

USE eventosuniversidad;

/* crear las tablas */

  -- estudiantes 

  CREATE OR REPLACE TABLE estudiantes(
    codigoEstudiante varchar(9),
    nombreCompleto varchar(50),
    dniEstudiante varchar(9), 
    direccion varchar(50),
    telefono varchar(9), 
    genero varchar(10),
    fechaNacimiento date, 
    idCarrera varchar(9), 
    PRIMARY KEY (codigoEstudiante)
  );

-- profesores

  CREATE OR REPLACE TABLE profesores(
    codigoProfesor varchar(9),
    dniProfesor  varchar(9), 
    nombreCompleto varchar(50),
    direccion varchar(50),
    telefono varchar(9), 
    nivelEducación varchar(25),
    fechaNacimiento date, 
    correo varchar (50),
    PRIMARY KEY (codigoProfesor)
  );

-- carreras

  CREATE OR REPLACE TABLE carreras(
    codigoCarrera varchar(9),
    nombreCarrera varchar(100),
    facultad varchar (150),
    PRIMARY KEY(codigoCarrera)
  );
 
  -- talleres 
  CREATE OR REPLACE TABLE talleres(
    codigoTaller varchar (9), 
    nombreTaller varchar (255),
    fecha date,
    horaEntrada time,
    horaSalida time,
    precio decimal(10,2),
    ubicacion text,
    cupoMaximo int, 
    idCarrera varchar(9), 
    PRIMARY KEY(codigoTaller)
  );

  -- conferencias 

    CREATE OR REPLACE TABLE conferencias(
      codigoConferencia varchar(9),
      nombreConferencia varchar(100), 
      precio decimal (10,2), 
      fecha date,
      horaEntrada time,
      horaSalida time,
      cupoMaximo int,
      ubicación varchar(50),
      idCarrera varchar(9), 
      PRIMARY KEY(codigoConferencia)
      ); 

    -- temas

  CREATE OR REPLACE TABLE temas (
      codigoTema varchar (9),
      nombreTema varchar (100), 
      PRIMARY KEY (codigoTema)
  );

  -- conferencias-temas 

   CREATE OR REPLACE TABLE impartenconferencias(
      id int AUTO_INCREMENT,
      idConferencia varchar(9),
      idTema varchar(9),
      PRIMARY KEY(id)

);
  CREATE OR REPLACE TABLE asistentalleres(
    id int AUTO_INCREMENT,
    idEstudiante varchar(9), 
    idTaller varchar(9), 
    horaEntrada time, 
    horaSalida time,
    PRIMARY KEY(id)

  ); 

    CREATE OR REPLACE TABLE asistenconferencias(
    id int AUTO_INCREMENT,
    idEstudiante varchar(9), 
    idConferencia varchar(9), 
    horaEntrada time, 
    horaSalida time,
    PRIMARY KEY(id)

  ); 

     CREATE OR REPLACE TABLE expositores(
    
    codigoExpositor varchar(9), 
    dniExpositor varchar(9),
    nombreExpositor varchar(50), 
    nacionalidad varchar(50),
    genero varchar(10),
    correo varchar(50),
    fechaNacimiento date, 
    telefono varchar(9),
    PRIMARY KEY(codigoExpositor)

  ); 

  CREATE OR REPLACE TABLE exponenConferencias(
    id int AUTO_INCREMENT,
    idConferencia varchar(9),
    idExpositor varchar(9),
    PRIMARY KEY (id)
  );

  CREATE OR REPLACE TABLE impartenTalleres(
    id int AUTO_INCREMENT,
    idProfesor varchar(9),
    idTaller varchar(9),
    PRIMARY KEY(id)
  );

  CREATE OR REPLACE TABLE correos(
    id int AUTO_INCREMENT,
    idEstudiante varchar(9),
    correo varchar (50),
    contrasena varchar(10)
    PRIMARY KEY (id)
  );
/** creacion de las restricciones **/

-- talleres
  ALTER TABLE talleres
  ADD CONSTRAINT fk_carreras_talleres
  FOREIGN KEY (idCarrera)
  REFERENCES carreras(codigoCarrera);

-- estudiantes
  ALTER TABLE estudiantes
  ADD CONSTRAINT fk_estudiante_carrera
  FOREIGN KEY (idCarrera)
  REFERENCES carreras(codigoCarrera);


  -- conferencias_temas
  ALTER TABLE impartenconferencias
  ADD CONSTRAINT fk_imparten__temas
  FOREIGN KEY (idTema)
  REFERENCES temas(codigoTema),

  ADD CONSTRAINT fk_imparten_conferencias
  FOREIGN KEY (idConferencia)
  REFERENCES conferencias(codigoConferencia),

  ADD CONSTRAINT uk_imparten_conferencias_temas
  UNIQUE KEY (idTema,idConferencia);

--


  -- asistenTaller 
  ALTER TABLE asistentalleres 
  ADD CONSTRAINT fk_asistentaller_taller
  FOREIGN KEY (idTaller)
  REFERENCES talleres(codigoTaller); 

  ALTER TABLE asistentalleres 
  ADD CONSTRAINT fk_aistentaller_estudiante
  FOREIGN KEY (idEstudiante)
  REFERENCES estudiantes(codigoEstudiante),

  ADD CONSTRAINT uk_asisten_taller
  UNIQUE KEY (idTaller,idEstudiante);

 -- asistenConferencia

  ALTER TABLE asistenconferencias
  ADD CONSTRAINT fk_asistenconferencias
  FOREIGN KEY (idConferencia)
  REFERENCES conferencias(codigoConferencia); 

  ALTER TABLE asistenconferencias
  ADD CONSTRAINT fk_asistenconferencia_estudiante
  FOREIGN KEY (idEstudiante)
  REFERENCES estudiantes(codigoEstudiante),

  ADD CONSTRAINT uk_asisten_conferencia
  UNIQUE KEY (idConferencia,idEstudiante);

-- impartenTalleres

  ALTER TABLE impartenTalleres
  ADD CONSTRAINT fk_impartenTalleres_Profesor
  FOREIGN KEY (idProfesor)
  REFERENCES profesores(codigoProfesor); 

  ALTER TABLE impartenTalleres
  ADD CONSTRAINT fk_impartenTalleres_Talleres
  FOREIGN KEY (idTaller)
  REFERENCES talleres(codigoTaller),

  ADD CONSTRAINT uk_imparten_talleres
  UNIQUE KEY (idProfesor,idTaller);

-- exponenConferencias

  ALTER TABLE exponenConferencias
  ADD CONSTRAINT fk_exponen_conferencia
  FOREIGN KEY (idConferencia)
  REFERENCES conferencias(codigoConferencia); 

  ALTER TABLE exponenConferencias
  ADD CONSTRAINT fk_exponen_expositor
  FOREIGN KEY (idExpositor)
  REFERENCES expositores(codigoExpositor),

  ADD CONSTRAINT uk_exponen_conferencias
  UNIQUE KEY (idConferencia,idExpositor);

    -- conferencias
  ALTER TABLE conferencias
  ADD CONSTRAINT fk_carreras_conferencias
  FOREIGN KEY (idCarrera)
  REFERENCES carreras(codigoCarrera);
  

-- correos

   ALTER TABLE correos
  ADD CONSTRAINT fk_correos_estudiantes
  FOREIGN KEY (idEstudiante)
  REFERENCES estudiantes(codigoEstudiante), 

  ADD CONSTRAINT uk_estudiantes_correos
  UNIQUE KEY(idEstudiante,correo); 

  -- profesores 
  ALTER TABLE profesores
  ADD CONSTRAINT uk_profesores
  UNIQUE KEY (dniProfesor); 

-- expositores

  ALTER TABLE expositores
  ADD CONSTRAINT uk_expositores
  UNIQUE KEY (dniExpositor); 
-- estudiantes 
   ALTER TABLE estudiantes
  ADD CONSTRAINT uk_estudiantes
  UNIQUE KEY (dniEstudiante);

-- Disable foreign key checks
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;

INSERT INTO carreras(codigoCarrera, nombreCarrera, facultad) VALUES
('CR1285172', 'Ingeniería Informática', 'Facultad de Ingeniería y Ciencias de la Computación'),
('CR0255686', 'Medicina', 'Facultad de Medicina'),
('CR4580047', 'Derecho', 'Facultad de Derecho y Ciencias Sociales'),
('CR4607313', 'Economía', 'Facultad de Economía y Negocios'),
('CR6443262', 'Psicología', 'Facultad de Ciencias de la Salud Mental'),
('CR1449832', 'Arquitectura', 'Facultad de Arquitectura y Diseño'),
('CR1651922', 'Biología', 'Facultad de Ciencias Naturales'),
('CR5541744', 'Matemáticas', 'Facultad de Ciencias Exactas'),
('CR6683184', 'Química', 'Facultad de Ciencias Químicas'),
('CR5924796', 'Historia', 'Facultad de Humanidades'),
('CR3127747', 'Ciencias de la Computación', 'Facultad de Ingeniería y Ciencias de la Computación'),
('CR7072349', 'Medicina Veterinaria ', 'Facultad de Ciencias Veterinarias'),
('CR2333749', 'Psicología Clínica', 'Facultad de Ciencias de la Salud Mental'),
('CR4574567', 'Ingeniería Civil', 'Facultad de Ingeniería y Ciencias de la Construcción'),
('CR8252817', 'Arte y Diseño', 'Facultad de Arquitectura y Diseño'),
('CR6532752', 'Historia del Arte', 'Facultad de Humanidades'),
('CR4933541', 'Ciencias Políticas', 'Facultad de Ciencias Políticas y Sociales'),
('CR8250410', 'Biología Marina', 'Facultad de Ciencias del Mar'),
('CR0970427', 'Ciencias Ambientales', 'Facultad de Ciencias Ambientales'),
('CR3919253', 'Ingeniería Eléctrica', 'Facultad de Ingeniería Eléctrica y Electrónica'),
('CR7167911', 'Ingeniería Mecánica', 'Facultad de Ingeniería Mecánica'),
('CR2197080', 'Física', 'Facultad de Ciencias Físicas'),
('CR5162751', 'Matemáticas Aplicadas', 'Facultad de Ciencias Matemáticas'),
('CR8063154', 'Arquitectura Paisajística', 'Facultad de Arquitectura y Diseño'),
('CR7755202', 'Ingeniería Química', 'Facultad de Ingeniería Química'),
('CR8777038', 'Nutrición y Dietética', 'Facultad de Ciencias de la Nutrición'),
('CR9827423', 'Enfermería', 'Facultad de Ciencias de la Salud'),
('CR4684390', 'Odontología', 'Facultad de Odontología'),
('CR7898613', 'Turismo', 'Facultad de Turismo y Hotelería'),
('CR1018166', 'Ciencias del Deporte', 'Facultad de Ciencias del Deporte');

--
-- Inserting data into table expositores
--
INSERT INTO expositores(codigoExpositor, dniExpositor, nombreExpositor, nacionalidad, genero, correo, fechaNacimiento, telefono) VALUES
('EX8146564', '39627856H', 'María González Pérez', 'Mexico', 'Femenino', 'maria.gonzalez@example.com', '1965-01-01', '636408927'),
('EX9662806', '24204713X', 'Juan Martínez Rodríguez', 'Israel', 'Masculino', 'juan.martinez@example.com', '1971-06-11', '653888024'),
('EX9733187', '15162946Q', 'Ana Rodríguez Martínez', 'Nicaragua', 'Masculino', 'ana.rodriguez@example.com', '1988-04-07', '696649260'),
('EX2242041', '90367264A', 'Carlos Pérez López', 'Uganda', 'Femenino', 'carlos.perez@example.com', '1987-10-27', '642014836'),
('EX4783268', '40848247R', 'Laura López González', 'Saudi Arabia', 'Masculino', 'laura.lopez@example.com', '1973-02-10', '695563425'),
('EX0548164', '34706818T', 'David Sánchez Martín', 'Mongolia', 'Femenino', 'david.sanchez@example.com', '1983-04-08', '656291182'),
('EX6801815', '13407119Y', 'Sofía García Díaz', 'Nicaragua', 'Femenino', 'sofia.garcia@example.com', '1966-07-17', '682546388'),
('EX4002198', '97547725R', 'Miguel Fernández Sánchez', 'Fiji', 'Masculino', 'miguel.fernandez@example.com', '1965-01-06', '669946719'),
('EX7371766', '48011838I', 'Lucía Díaz García', 'Nigeria', 'Femenino', 'lucia.diaz@example.com', '1965-01-09', '600921860'),
('EX5344568', '00438004J', 'Pedro Martín Martínez', 'Ukraine', 'Masculino', 'pedro.martin@example.com', '1971-07-29', '607444574'),
('EX5501805', '15487536G', 'Marta Gómez López', 'Croatia', 'Masculino', 'marta.gomez@example.com', '1965-03-04', '695735420'),
('EX2502572', '04530488V', 'Javier Ruiz García', 'Israel', 'Femenino', 'javier.ruiz@example.com', '1967-01-12', '662785969'),
('EX8623453', '10096513X', 'Elena Hernández Pérez', 'Austria', 'Masculino', 'elena.hernandez@example.com', '1968-08-23', '654883858'),
('EX1660128', '26368155A', 'Francisco Pérez Rodríguez', 'Guyana', 'Femenino', 'francisco.perez@example.com', '1974-02-20', '643620653'),
('EX3616273', '96064096I', 'Andrea López Martín', 'Botswana', 'Femenino', 'andrea.lopez@example.com', '1966-10-02', '646183341'),
('EX7278003', '54653177N', 'José Sánchez Díaz', 'Ukraine', 'Masculino', 'jose.sanchez@example.com', '1965-01-11', '627748243'),
('EX7526182', '35448763D', 'Laura García López', 'Luxembourg', 'Masculino', 'laura.garcia@example.com', '1982-05-15', '660358804'),
('EX5769488', '02704296I', 'Pablo Martínez González', 'Belize', 'Masculino', 'pablo.martinez@example.com', '1977-08-16', '671509477'),
('EX0316562', '64632700X', 'Cristina Rodríguez Martín', 'Kenya', 'Masculino', 'cristina.rodriguez@example.com', '1971-08-09', '605581707'),
('EX2545082', '00253568I', 'Manuel Pérez López', 'Guatemala', 'Femenino', 'manuel.perez@example.com', '1965-02-12', '650395893'),
('EX0767947', '45792942G', 'Natalia López Martínez', 'Lithuania', 'Femenino', 'natalia.lopez@example.com', '1965-02-14', '627869035'),
('EX1116948', '71312374B', 'Gabriel Martín Sánchez', 'Kenya', 'Femenino', 'gabriel.martin@example.com', '1984-05-27', '649340636'),
('EX1985817', '59391383D', 'Claudia Sánchez García', 'Turkey', 'Femenino', 'claudia.sanchez@example.com', '1973-08-10', '697896270'),
('EX8017463', '42810868I', 'Rafael González Rodríguez', 'Namibia', 'Masculino', 'rafael.gonzalez@example.com', '1984-03-03', '683427691'),
('EX5854236', '50502944U', 'Victoria Díaz Martín', 'Mauritania', 'Masculino', 'victoria.diaz@example.com', '1967-01-06', '620952215'),
('EX2042992', '56372594Y', 'Luis Martínez López', 'Georgia', 'Femenino', 'luis.martinez@example.com', '1976-10-28', '647162083'),
('EX3354128', '76412235Q', 'Paula Rodríguez Díaz', 'Indonesia', 'Masculino', 'paula.rodriguez@example.com', '1987-12-20', '610225767'),
('EX8483380', '05580030U', 'Álvaro Pérez García', 'United States', 'Masculino', 'alvaro.perez@example.com', '1980-02-02', '685649196'),
('EX4228097', '23455475Z', 'Martina López Martínez', 'Germany', 'Femenino', 'martina.lopez@example.com', '1980-09-12', '658950671'),
('EX5317572', NULL, 'Andrés Sánchez Rodríguez', 'Argentina', 'Masculino', 'andres.sanchez@example.com', '1986-10-30', '685325712');

--
-- Inserting data into table profesores
--
INSERT INTO profesores(codigoProfesor, dniProfesor, nombreCompleto, direccion, telefono, nivelEducación, fechaNacimiento, correo) VALUES
('PR1397132', '23943212N', 'Martina Rodríguez Pérez', 'Avenida del Río, #567, Residencial Los Pájaros', '613519804', 'Licenciatura', '1986-10-02', 'martina.gonzalez@example.com'),
('PR8742652', '17454560Y', 'Alejandro Martínez Ruiz', 'Callejón del Río, #789, Villa del Carmen', '632620601', 'Licenciatura', '1971-03-30', 'alejandro.martinez@example.com'),
('PR5262142', '17736527Y', 'Sofía Díaz Hernández', 'Avenida de las Acacias, #567, Residencial Los Alpe', '661144202', 'Licenciatura', '1975-09-13', 'sofia.rodriguez@example.com'),
('PR2779524', '98790053V', 'Juan Pérez López', 'Avenida de los Girasoles, #123, Urbanización Los J', '682929660', 'Maestría', '1970-10-03', 'juan.perez@example.com'),
('PR2487774', '09392566W', 'Valentina Gómez Martín', 'Calle del Bosque, #456, Los Laureles', '643418118', 'Doctorado', '1972-02-14', 'valentina.lopez@example.com'),
('PR9493470', '78631903H', 'Lucas Sánchez Gómez', 'Boulevard de las Estrellas, #234, Colonia Santa Ro', '680175375', 'Doctorado', '1986-07-28', 'lucas.sanchez@example.com'),
('PR7486918', '50641061Y', 'Emma Pérez Rodríguez', 'Calle de las Acacias, #456, Barrio San Miguel', '625733072', 'Maestría', '1985-03-30', 'emma.garcia@example.com'),
('PR6761699', '32367714Q', 'Mateo Martín Sánchez', 'Avenida de las Mariposas, #567, Residencial Los Co', '681085690', 'Licenciatura', '1982-04-07', 'mateo.martin@example.com'),
('PR3167416', '24027396K', 'Valeria López Pérez', 'Avenida de las Palmas, #123, Colonia San José', '679194572', 'Doctorado', '1970-01-01', 'valeria.lopez@example.com'),
('PR4157895', '82658627L', 'Santiago Díaz Ruiz', 'Boulevard de las Palmas, #789, Villa de las Colina', '661399006', 'Maestría', '1976-06-12', 'santiago.ruiz@example.com'),
('PR0414120', '00632078B', 'Isabella Gómez Martínez', 'Calle de los Abedules, #890, Barrio San Marcos', '631459471', 'Doctorado', '1989-02-28', 'isabella.gomez@example.com'),
('PR5651003', '70238113X', 'Leonardo Sánchez Hernández', 'Calle del Mar, #456, Ciudad del Sol', '652987290', 'Doctorado', '1978-02-22', 'leonardo.hernandez@example.com'),
('PR3530459', '15022735L', 'Julia Rodríguez Díaz', 'Calle de las Estrellas, #890, Colonia San Antonio', '674735082', 'Licenciatura', '1984-10-02', 'julia.rodriguez@example.com'),
('PR0301251', '55777847F', 'Mateo Martínez Pérez', 'Boulevard de los Pájaros, #234, Barrio Primavera', '691266707', 'Doctorado', '1982-11-24', 'mateo.martinez@example.com'),
('PR2539800', '21549614R', 'Valentina Sánchez Gómez', 'Paseo de los Robles, #234, Ciudad de las Flores', '640480982', 'Maestría', '1984-02-11', 'valentina.sanchez@example.com'),
('PR4483160', '71184076R', 'Lucas Pérez Martín', 'Calle de los Pinos, #890, Urbanización Los Cedros', '616493477', 'Maestría', '1972-12-11', 'lucas.perez@example.com'),
('PR5745229', '62229649L', 'Martina Díaz López', 'Avenida de las Orquídeas, #567, Residencial Las Fl', '688289837', 'Maestría', '1970-09-07', 'martina.diaz@example.com'),
('PR7946000', '21396423K', 'Diego Gómez Rodríguez', 'Avenida de los Olivos, #123, Urbanización Las Rosa', '638977130', 'Maestría', '1990-08-13', 'diego.gomez@example.com'),
('PR9887070', '04860169Z', 'Emma Sánchez Martínez', 'Calle de la Rosa, #123, Ciudad Jardín', '694520084', 'Doctorado', '1988-03-04', 'emma.sanchez@example.com'),
('PR9642799', '20317686U', 'Sebastián López Gómez', 'Paseo de los Cerezos, #789, Barrio San Martín', '636135353', 'Licenciatura', '1989-08-22', 'sebastian.lopez@example.com'),
('PR0551487', '58652708T', 'Sofía Rodríguez Martín', 'Paseo de los Abedules, #789, Pueblo Viejo', '638728664', 'Maestría', '1970-01-06', 'sofia.rodriguez@example.com'),
('PR0561897', '34278582O', 'Nicolás Pérez Díaz', 'Calle de los Cerezos, #234, Villa de la Montaña', '684916857', 'Maestría', '1977-10-01', 'nicolas.perez@example.com'),
('PR0533981', '32536894E', 'Isabella Martínez Sánchez', 'Calle de los Lirios, #890, Barrio Los Álamos', '635533595', 'Doctorado', '1980-02-11', 'isabella.martinez@example.com'),
('PR1574194', '65918701P', 'Santiago Gómez López', 'Avenida de las Rosas, #123, Ciudad Nueva', '677112137', 'Maestría', '1984-03-18', 'santiago.gomez@example.com'),
('PR7301479', '38014190X', 'Valeria Martín Rodríguez', 'Callejón de las Mariposas, #234, Villa Bella', '652337889', 'Doctorado', '1986-04-01', 'valeria.martin@example.com'),
('PR9750873', '41657863W', 'Mateo Díaz Gómez', 'Avenida de los Robles, #567, Urbanización El Bosqu', '607687719', 'Licenciatura', '1972-03-07', 'mateo.diaz@example.com'),
('PR5099859', '86169172E', 'Julia Sánchez Pérez', 'Avenida del Sol, #456, Pueblo Nuevo', '612422490', 'Licenciatura', '1981-10-29', 'julia.sanchez@example.com'),
('PR1105377', '15924283N', 'Leonardo Martín Díaz', 'Calle de las Flores, #890, Residencial Los Pinos', '642218768', 'Licenciatura', '1971-04-06', 'leonardo.martin@example.com'),
('PR7859653', '36164763E', 'Lucas López Ruiz', 'Calle Principal, #789, Villa Esperanza', '654032024', 'Maestría', '1989-12-02', 'lucas.lopez@example.com'),
('PR9767130', '70402815J', 'Emma Rodríguez Sánchez', 'Paseo de los Olivos, #456, Barrio San Francisco', '620044224', 'Licenciatura', '1988-07-15', 'emma.rodriguez@example.com');

--
-- Inserting data into table temas
--
INSERT INTO temas(codigoTema, nombreTema) VALUES
('TE2303955', 'Diseño de Espacios Exteriores y su Impacto en el Medio Ambiente'),
('TE1187460', 'Historia del Arte: Movimientos Artísticos del Siglo XX'),
('TE1043394', 'Energías Renovables y su Contribución a la Sostenibilidad'),
('TE4794169', 'Política y Gobernanza: Retos Actuales y Futuros'),
('TE1311443', 'Avances en Energía Eléctrica y Tecnologías de Redes Inteligentes'),
('TE2530035', 'Innovaciones en Diseño de Maquinaria y Sistemas Mecánicos'),
('TE7554912', 'Turismo Sostenible y Experiencias de Viaje Responsables'),
('TE1735561', 'Aplicaciones Prácticas de la Teoría de Grafos en Ingeniería'),
('TE9793586', 'Procesos Químicos Avanzados para la Industria'),
('TE8900866', 'Física Cuántica: Aplicaciones Prácticas en la Tecnología Actual'),
('TE0164248', 'Avances en Odontología: Nuevas Tecnologías y Procedimientos'),
('TE5025636', 'Explorando la Biodiversidad Marina y su Conservación'),
('TE1791152', 'Nutrición y Dietética: Estrategias para una Alimentación Saludable'),
('TE4811453', 'Modelado Matemático en Ciencias de la Salu'),
('TE7798916', 'El Arte Contemporáneo y su Relevancia en la Sociedad Actual'),
('TE8020269', 'Avances en Medicina Veterinaria: Cuidado de Animales Exóticos'),
('TE1578791', 'Gestión Ambiental y Desarrollo Sostenible en la Industria'),
('TE2651882', 'Tendencias Económicas Globales y Oportunidades de Inversión'),
('TE7756472', 'Innovaciones en la Síntesis de Compuestos Químicos'),
('TE1149240', 'Avances en Inteligencia Artificial y su Impacto en la Industria'),
('TE6759697', 'Tendencias y Desafíos en el Cuidado de Enfermería'),
('TE8913392', 'Intervenciones Psicológicas Efectivas para el Estrés y la Ansiedad'),
('TE2531375', 'Reinterpretando la Historia: Perspectivas Contemporáneas'),
('TE0311120', 'Mindfulness y Salud Mental en el Entorno Laboral'),
('TE4649707', 'Innovaciones en la Construcción Sostenible y Eficiente'),
('TE1947905', 'Innovaciones en Cirugía Mínimamente Invasiva'),
('TE7374640', 'Sostenibilidad y Diseño Bioclimático en la Arquitectura'),
('TE5273866', 'Desafíos Legales en la Era Digital: Protección de Datos y Privacidad'),
('TE9553119', 'Nuevas Tendencias en Genética y Terapias Genéticas'),
('TE3400186', 'Avances en Seguridad Cibernética y Protección de Datos');

--
-- Inserting data into table conferencias
--
INSERT INTO conferencias(codigoConferencia, nombreConferencia, precio, fecha, cupoMaximo, ubicación, idCarrera, horaEntrada, horaSalida) VALUES
('CON725127', 'Ciencias del Deporte y Rendimiento Humano', 181.89, '2024-03-23', 181, 'Espacio de Innovación Tecnológica', 'CR1018166', '09:00:00', '09:00:00'),
('CON209656', 'Derecho en la Era Digital: Desafíos y Oportunidades', 200, '2024-07-11', 200, 'Sala de Medios o Audiovisuales', 'CR1285172', '10:00:00', '19:00:00'),
('CON896791', 'Arte y Diseño Contemporáneo: Explorando la Creatividad', 153.79, '2023-11-11', 153, 'Sala de Proyectos o Diseño', 'CR8252817', '08:00:00', '19:00:00'),
('CON413161', 'Política Global y Desafíos de Gobernanza', 155.47, '2024-05-10', 155, 'Galería de Arte', 'CR4580047', '09:00:00', '20:00:00'),
('CON335701', 'Modelado Matemático en Ciencias de la Salud', 195.18, '2024-05-09', 195, 'Auditorio', 'CR0255686', '10:00:00', '20:30:00'),
('CON182327', 'Avances en la Medicina Veterinaria: Cuidado Animal', 194, '2024-07-11', 194, 'Laboratorio de Ciencias', 'CR7072349', '10:00:00', '20:30:00'),
('CON330283', 'Inteligencia Artificial y Futuro de la Tecnología', 181.68, '2023-12-21', 181, 'Sala de Psicología o Consejería', 'CR1285172', '08:00:00', '18:00:00'),
('CON193401', 'Biodiversidad y Conservación en la Biología Moderna', 178.76, '2024-01-16', 178, 'Taller de Mecánica o Ingeniería', 'CR1651922', '10:00:00', '19:00:00'),
('CON929010', 'Química Avanzada: Descubrimientos y Aplicaciones', 190.81, '2023-12-11', 190, 'Auditorio', 'CR6683184', '09:00:00', '19:00:00'),
('CON421099', 'Avances en Enfermería: Prácticas de Atención al Paciente', 161.74, '2024-04-09', 161, 'Sala de Experimentos', 'CR9827423', '10:00:00', '19:00:00'),
('CON625294', 'Psicología Clínica: Nuevos Enfoques Terapéuticos', 195.42, '2024-01-12', 195, 'Gimnasio o Sala de Deportes', 'CR6443262', '10:00:00', '19:00:00'),
('CON182002', 'Arquitectura Paisajística y Diseño Sostenible', 194.29, '2024-05-29', 194, 'Laboratorio de Ingeniería', 'CR8063154', '09:00:00', '19:00:00'),
('CON313035', 'Revoluciones y Transformaciones en la Historia', 164.43, '2024-01-08', 164, 'Centro de Idiomas', 'CR5924796', '10:00:00', '19:00:00'),
('CON467223', 'Ingeniería Mecánica Innovadora y Diseño de Productos', 164.54, '2024-02-29', 164, 'Sala de Informática', 'CR7167911', '10:00:00', '21:00:00'),
('CON658884', 'Explorando los Ecosistemas Marinos y su Conservación', 154.79, '2024-01-04', 154, 'Espacio de Investigación o Desarrollo', 'CR7072249', '08:00:00', '19:00:00'),
('CON804461', 'Odontología Moderna: Tecnologías y Tratamientos Innovadores', 150.62, '2024-02-17', 150, 'Aula de Clases', 'CR4684390', '10:00:00', '19:00:00'),
('CON227442', 'Economía Global: Perspectivas y Tendencias Futuras', 178.57, '2024-01-01', 178, 'Espacio para Presentaciones o Pitching', 'CR4607313', '10:00:00', '19:00:00'),
('CON349663', 'Ingeniería Química Avanzada y Procesos Industriales', 162.19, '2024-06-13', 162, 'Teatro Universitario', 'CR775502', '09:00:00', '21:00:00'),
('CON386190', 'Turismo Sostenible y Experiencias de Viaje Responsables', 190.77, '2024-07-12', 190, 'Sala de Conferencias', 'CR7898613', '08:00:00', '19:30:00'),
('CON704273', 'Ingeniería Civil del Futuro: Innovación y Sostenibilidad', 167.16, '2023-11-24', 167, 'Centro de Idiomas', 'CR4574567', '09:00:00', '19:00:00'),
('CON607835', 'Matemáticas Aplicadas: Soluciones para el Mundo Real', 157.44, '2023-11-18', 157, 'Laboratorio de Informática', 'CR5162751', '10:00:00', '19:00:00'),
('CON226158', 'Psicología Positiva y Bienestar en el Trabajo', 151.41, '2023-11-30', 151, 'Sala de Experimentos', 'CR6443262', '10:00:00', '20:30:00'),
('CON835816', 'Ciberseguridad y Privacidad en la Era Digital', 176.7, '2024-03-27', 176, 'Estudio de Arte o Taller Creativo', 'CR1285172', '08:00:00', '19:00:00'),
('CON519917', 'Física Cuántica: Avances y Aplicaciones Tecnológicas', 178.51, '2024-03-24', 178, 'Sala de Estudio Grupal', 'CR2197080', '10:00:00', '19:00:00'),
('CON443676', 'Nutrición y Dietética: Salud y Bienestar Nutricional', 166.43, '2024-01-13', 166, 'Biblioteca Universitaria', 'CR8777033', '10:00:00', '21:00:00'),
('CON311969', 'Ciencias Ambientales: Soluciones para un Futuro Sostenible', 191.28, '2023-11-12', 191, 'Centro de Innovación o Incubadora de Empresas', 'CR0970427', '08:00:00', '19:00:00'),
('CON659644', 'Historia del Arte: Perspectivas Multiculturales', 160.76, '2024-02-16', 160, 'Auditorio', 'CR6532752', '10:00:00', '19:00:00'),
('CON887858', 'Innovación y Tecnología en la Medicina Moderna', 156.83, '2024-01-24', 156, 'Sala de Informática', 'CR0255686', '10:00:00', '20:30:00'),
('CON417791', 'Arquitectura Sostenible: Diseñando el Futuro', 196.39, '2023-11-14', 196, 'Laboratorio de Ciencias', 'CR1449832', '10:00:00', '19:00:00'),
('CON750780', 'Ingeniería Eléctrica: Energía para el Mundo Moderno', 199.08, '2024-01-05', 199, 'Sala de Reuniones', 'CR3919253', '08:00:00', '19:00:00');

--
-- Inserting data into table estudiantes
--
INSERT INTO estudiantes(codigoEstudiante, nombreCompleto, dniEstudiante, direccion, telefono, genero, fechaNacimiento, idCarrera) VALUES
('EU9473924', 'Alejandro Martínez Ruiz', '16153085U', 'Avenida de las Acacias, #567, Residencial Los Alpes', '608741080', 'Femenino', '1998-05-25', 'CR7167911'),
('EU8602252', 'Emma Rodríguez Sánchez', '13297988X', 'Calle de los Abedules, #890, Barrio San Marcos', '651517229', 'Masculino', '1997-04-20', 'CR7898613'),
('EU0018912', 'Mateo Díaz Gómez', '95127874H', 'Calle del Bosque, #456, Los Laureles', '674661949', 'Femenino', '1995-12-07', 'CR1018166'),
('EU4629155', 'Isabella Gómez Martínez', '10434088V', 'Paseo de los Abedules, #789, Pueblo Viejo', '679056926', 'Masculino', '1990-01-03', 'CR5924796'),
('EU1270622', 'Isabella Martínez Sánchez', '77356870C', 'Avenida de los Olivos, #123, Urbanización Las Rosa', '657500246', 'Masculino', '2000-12-05', 'CR7167911'),
('EU1785651', 'Lucas López Ruiz', '73790593K', 'Avenida del Río, #567, Residencial Los Pájaros', '688998751', 'Masculino', '1992-04-15', 'CR5924796'),
('EU9855132', 'Lucas Sánchez Gómez', '77565324D', 'Calle de los Pinos, #890, Urbanización Los Cedros', '669320627', 'Femenino', '1997-07-23', 'CR7898613'),
('EU4832093', 'Mateo Martín Sánchez', '18439761Z', 'Calle de las Flores, #890, Residencial Los Pinos', '645318219', 'Masculino', '2001-01-19', 'CR8250410'),
('EU3337198', 'Leonardo Hernández Sánchez', '29860373Y', 'Calle de los Cerezos, #234, Villa de la Montaña', '687723259', 'Masculino', '1993-11-13', 'CR7755202'),
('EU4662638', 'Lucas Pérez Martín', '28040267B', 'Calle Principal, #789, Villa Esperanza', '614188157', 'Masculino', '2002-11-12', 'CR4574567'),
('EU2522398', 'Sofía García Hernández', '36685202C', 'Calle de los Lirios, #890, Barrio Los Álamos', '644620938', 'Masculino', '1990-11-19', 'CR4574567'),
('EU8203469', 'Sebastián López Gómez', '11297181D', 'Callejón del Río, #789, Villa del Carmen', '699122994', 'Femenino', '1994-04-30', 'CR7898613'),
('EU8660850', 'Martina Díaz López', '35848793K', 'Calle de las Acacias, #456, Barrio San Miguel', '695290234', 'Femenino', '2003-11-27', 'CR7898613'),
('EU0073462', 'Emma Pérez Rodríguez', '42143101G', 'Avenida de las Mariposas, #567, Residencial Los Colinas', '644391367', 'Masculino', '2002-07-10', 'CR7755202'),
('EU4477511', 'Sofía Rodríguez Martín', '16553988R', 'Avenida de las Palmas, #123, Colonia San José', '678323905', 'Masculino', '2001-07-08', 'CR1651922'),
('EU1489516', 'Valeria López Pérez', '27066566X', 'Boulevard de los Pájaros, #234, Barrio Primavera', '674364128', 'Masculino', '1998-09-27', 'CR7167911'),
('EU1226513', 'Juan Rodríguez López', '14066210P', 'Boulevard de las Palmas, #789, Villa de las Colina', '669191883', 'Femenino', '1992-02-06', 'CR3127747'),
('EU3750103', 'Julia Rodríguez Díaz', '27812892M', 'Paseo de los Cerezos, #789, Barrio San Martín', '660445928', 'Masculino', '1993-11-12', 'CR8252817'),
('EU8502442', 'Martina González Pérez', '59336919S', 'Avenida de los Girasoles, #123, Urbanización Los Juanjos', '676061508', 'Femenino', '1993-06-27', 'CR0970427'),
('EU2040107', 'Diego Gómez Rodríguez', '60982653X', 'Boulevard de las Estrellas, #234, Colonia Santa Rosa', '683443363', 'Masculino', '1992-02-26', 'CR2197080'),
('EU5326858', 'Julia Sánchez Pérez', '57168559H', 'Calle del Mar, #456, Ciudad del Sol', '627092084', 'Masculino', '2001-03-19', 'CR8777038'),
('EU1090668', 'Santiago Gómez López', '81447384R', 'Calle de las Estrellas, #890, Colonia San Antonio', '674417765', 'Masculino', '1990-04-06', 'CR1651922'),
('EU6191317', 'Valentina Sánchez Gómez', '84830158X', 'Calle de la Rosa, #123, Ciudad Jardín', '639727892', 'Masculino', '1997-05-16', 'CR8250410'),
('EU9828307', 'Leonardo Martín Díaz', '10253487M', 'Paseo de los Robles, #234, Ciudad de las Flores', '684679723', 'Femenino', '1994-06-17', 'CR5924796'),
('EU6054431', 'Valeria Martín Rodríguez', '82905425C', 'Avenida de las Orquídeas, #567, Residencial Las Flores', '693339461', 'Masculino', '1990-10-18', 'CR5162751'),
('EU3739178', 'Emma Sánchez Martínez', '90134940D', 'Avenida de las Rosas, #123, Ciudad Nueva', '619955569', 'Masculino', '1990-06-25', 'CR1651922'),
('EU2408099', 'Valentina Díaz Martín', '77834779K', 'Avenida de los Robles, #567, Urbanización El Bosque', '619636487', 'Masculino', '1998-07-03', 'CR4607313'),
('EU8259577', 'Santiago Ruiz Díaz', '23228818N', 'Avenida del Sol, #456, Pueblo Nuevo', '674667160', 'Femenino', '2000-03-04', 'CR7755202'),
('EU1703004', 'Nicolás Pérez Díaz', '12450218Z', 'Callejón de las Mariposas, #234, Villa Bella', '659832001', 'Femenino', '1996-07-14', 'CR5924796'),
('EU3202694', 'Mateo Martínez Pérez', '72576527G', 'Paseo de los Olivos, #456, Barrio San Francisco', '609601423', 'Femenino', '1995-08-03', 'CR7898613');

--
-- Inserting data into table talleres
--
INSERT INTO talleres(codigoTaller, nombreTaller, fecha, precio, ubicacion, cupoMaximo, idCarrera, horaEntrada, horaSalida) VALUES
('TA2025627', 'Taller de Ginecología y Obstetricia', '2024-01-09', 115.98, 'Espacio de Investigación o Desarrollo', 42, 'CR0255686', '09:00:00', '16:00:00'),
('TA1320590', 'Taller de Diseño de Paisajes', '2024-04-20', 123.24, 'Espacio para Presentaciones o Pitching', 41, 'CR8063154', '09:00:00', '16:00:00'),
('TA0441615', 'Taller de Circuitos Eléctricos', '2024-03-25', 132.69, 'Auditorio', 46, 'CR3919253', '10:00:00', '16:00:00'),
('TA8280130', 'Taller de Química Orgánica Experimental', '2024-04-05', 116.34, 'Sala de Informática', 47, 'CR6683184', '10:00:00', '16:00:00'),
('TA7174068', 'Taller de Procesos Químicos Industriales', '2023-12-10', 127.72, 'Laboratorio de Ciencias', 45, 'CR6683184', '10:00:00', '18:00:00'),
('TA4330881', 'Taller de Diseño de Máquinas', '2024-02-20', 98.07, 'Sala de Informática', 40, 'CR7167911', '10:00:00', '18:00:00'),
('TA2852440', 'Taller de Nutrición Clínica', '2023-11-30', 120.7, 'Espacio de Innovación Tecnológica', 44, 'CR8777033', '10:00:00', '18:00:00'),
('TA0845193', 'Taller de Terapia Cognitivo-Conductual', '2024-02-13', 114.58, 'Espacio para Presentaciones o Pitching', 48, 'CR6443262', '10:00:00', '18:00:00'),
('TA6359644', 'Taller de Seguridad Informática', '2023-12-01', 147.01, 'Centro de Idiomas', 42, 'CR1285172', '10:00:00', '18:00:00'),
('TA7044200', 'Taller de Ecología Marina', '2024-03-28', 137.49, 'Sala de Experimentos', 41, 'CR8250410', '10:00:00', '18:00:00'),
('TA4755328', 'Taller de Ingeniería Estructural', '2024-01-28', 94.63, 'Centro de Innovación o Incubadora de Empresas', 45, 'CR4574567', '10:00:00', '18:00:00'),
('TA0526006', 'Taller de Enfermería Pediátrica', '2023-12-30', 94.54, 'Laboratorio de Informática', 41, 'CR9827423', '10:00:00', '18:00:00'),
('TA8023762', 'Taller de Mecánica Cuántica', '2024-01-21', 135.64, 'Jardín Botánico o Espacio Verde', 45, 'CR7167911', '10:00:00', '18:00:00'),
('TA8903534', 'Taller de Radiología Diagnóstica', '2024-01-10', 136.02, 'Sala de Reuniones', 40, 'CR0255686', '10:00:00', '18:00:00'),
('TA0936898', 'Taller de Redes de Computadoras Avanzadas', '2024-02-04', 100.93, 'Sala de Medios o Audiovisuales', 44, 'CR1285172', '10:00:00', '18:00:00'),
('TA2494627', 'Taller de Cardiología Avanzada', '2024-04-16', 141.74, 'Sala de Psicología o Consejería', 44, 'CR0255686', '10:00:00', '18:00:00'),
('TA6780429', 'Taller de Historia Antigua', '2024-01-10', 147.68, 'Auditorio', 46, 'CR5924796', '10:00:00', '18:00:00'),
('TA3588315', 'Taller de Odontología Restauradora', '2024-04-11', 99.2, 'Sala de Experimentos', 41, 'CR4684390', '10:00:00', '18:00:00'),
('TA8885394', 'Taller de Patología Clínica', '2024-01-25', 124.74, 'Espacio para Presentaciones o Pitching', 44, 'CR0255686', '10:00:00', '18:00:00'),
('TA3955792', 'Taller de Programación en Python', '2024-04-07', 90.85, 'Gimnasio o Sala de Deportes', 42, 'CR1285172', '10:00:00', '18:00:00'),
('TA3779527', 'Taller de Escultura en Madera', '2023-12-30', 121.89, 'Laboratorio de Ingeniería', 46, 'CR8252817', '10:00:00', '18:00:00'),
('TA7105214', 'Taller de Desarrollo de Videojuegos', '2024-01-12', 112.87, 'Aula de Clases', 48, 'CR1285172', '10:00:00', '18:00:00'),
('TA8985969', 'Taller de Marketing Turístico', '2023-12-04', 96.71, 'Sala de Proyectos o Diseño', 48, 'CR7898613', '10:00:00', '18:00:00'),
('TA7551990', 'Taller de Desarrollo de Aplicaciones Móviles', '2024-01-07', 132.47, 'Centro de Idiomas', 47, 'CR1285172', '09:00:00', '18:00:00'),
('TA1217256', 'Taller de Computación en la Nube', '2024-04-19', 150, 'Galería de Arte', 40, 'CR1285172', '09:00:00', '18:00:00'),
('TA1403555', 'Taller de Inteligencia Artificial', '2023-11-14', 124.91, 'Teatro Universitario', 43, 'CR1285172', '09:00:00', '18:00:00'),
('TA8489496', 'Taller de Dermatología Práctica', '2024-01-25', 98.15, 'Estudio de Arte o Taller Creativo', 47, 'CR0255686', '09:00:00', '18:00:00'),
('TA2722468', 'Taller de Cirugía Laparoscópica', '2024-03-16', 126.88, 'Sala de Conferencias', 49, 'CR0255686', '09:00:00', '18:00:00'),
('TA3388732', 'Taller de Gestión Ambiental', '2024-01-26', 109.53, 'Espacio para Presentaciones o Pitching', 46, 'CR1651922', '09:00:00', '18:00:00'),
('TA0909196', 'Taller de Estadística Aplicada', '2024-05-05', 92.58, 'Biblioteca Universitaria', 46, 'CR5162751', '09:00:00', '18:00:00');

--
-- Inserting data into table asistenconferencias
--
INSERT INTO asistenconferencias(id, idEstudiante, idConferencia, horaEntrada, horaSalida) VALUES
(1, 'EU1703004', 'CON417791', '10:00:00', '19:00:00'),
(2, 'EU2040107', 'CON704273','09:00:00', '19:00:00'),
(3, 'EU8660850', 'CON313035', '10:00:00', '19:00:00'),
(4, 'EU1226513', 'CON227442', '10:00:00', '19:00:00'),
(5, 'EU4662638', 'CON421099', '10:00:00', '19:00:00'),
(6, 'EU0073462', 'CON467223', '10:00:00', '21:00:00'),
(7, 'EU0018912', 'CON896791', '08:00:00', '19:00:00'),
(8, 'EU9855132', 'CON330283', '08:00:00', '18:00:00'),
(9, 'EU4629155', 'CON413161', '09:00:00', '20:00:00'),
(10, 'EU2522398', 'CON625294', '10:00:00', '19:00:00'),
(11, 'EU9473924', 'CON725127', '09:00:00', '21:00:00'),
(12, 'EU4832093', 'CON193401', '10:00:00', '19:00:00'),
(13, 'EU3202694', 'CON750780', '08:00:00', '19:00:00'),
(14, 'EU3739178', 'CON311969', '08:00:00', '19:00:00'),
(15, 'EU6191317', 'CON835816', '08:00:00', '19:00:00'),
(16, 'EU2408099', 'CON659644', '10:00:00', '19:00:00'),
(17, 'EU8203469', 'CON182002', '09:00:00', '19:00:00'),
(18, 'EU3337198', 'CON929010', '09:00:00', '19:00:00'),
(19, 'EU1270622', 'CON335701', '10:00:00', '20:30:00'),
(20, 'EU5326858', 'CON607835', '10:00:00', '19:00:00'),
(21, 'EU8602252', 'CON209656', '10:00:00', '19:00:00'),
(22, 'EU9828307', 'CON519917', '10:00:00', '19:00:00'),
(23, 'EU1785651', 'CON182327', '10:00:00', '20:30:00'),
(24, 'EU8259577', 'CON887858', '10:00:00', '20:30:00'),
(25, 'EU3750103', 'CON349663', '09:00:00', '21:00:00'),
(26, 'EU6054431', 'CON443676', '10:00:00', '19:00:00'),
(27, 'EU4477511', 'CON658884', '10:00:00', '19:00:00'),
(28, 'EU8502442', 'CON386190', '09:00:00', '19:00:00'),
(29, 'EU1090668', 'CON226158', '10:00:00', '19:00:00'),
(30, 'EU1489516', 'CON804461', '10:00:00', '19:00:00');

--
-- Inserting data into table asistentalleres
--
INSERT INTO asistentalleres(id, idEstudiante, idTaller, horaEntrada, horaSalida) VALUES
(1, 'EU1703004', 'TA3388732', '02:00:00', '18:00:00'),
(2, 'EU2040107', 'TA3955792', '10:00:00', '18:00:00'),
(3, 'EU8660850', 'TA8023762', '10:00:00', '18:00:00'),
(4, 'EU1226513', 'TA6780429', '10:00:00', '18:00:00'),
(5, 'EU4662638', 'TA7044200', '10:00:00', '18:00:00'),
(6, 'EU0073462', 'TA8903534', '10:00:00', '18:00:00'),
(7, 'EU0018912', 'TA0441615', '10:00:00', '16:00:00'),
(8, 'EU9855132', 'TA2852440','10:00:00', '18:00:00'),
(9, 'EU4629155', 'TA8280130', '10:00:00', '16:00:00'),
(10, 'EU2522398', 'TA4755328', '10:00:00', '18:00:00'),
(11, 'EU9473924', 'TA2025627', '09:00:00', '16:00:00'),
(12, 'EU4832093', 'TA0845193', '10:00:00', '18:00:00'),
(13, 'EU3202694', 'TA0909196', '09:00:00', '18:00:00'),
(14, 'EU3739178', 'TA1403555', '09:00:00', '18:00:00'),
(15, 'EU6191317', 'TA8985969', '10:00:00', '18:00:00'),
(16, 'EU2408099', 'TA8489496','09:00:00', '18:00:00'),
(17, 'EU8203469', 'TA0526006', '10:00:00', '18:00:00'),
(18, 'EU3337198', 'TA6359644', '10:00:00', '18:00:00'),
(19, 'EU1270622', 'TA7174068', '10:00:00', '18:00:00'),
(20, 'EU5326858', 'TA3779527', '09:00:00', '16:00:00'),
(21, 'EU8602252', 'TA1320590', '09:00:00', '16:00:00'),
(22, 'EU9828307', 'TA7551990', '09:00:00', '18:00:00'),
(23, 'EU1785651', 'TA4330881', '10:00:00', '18:00:00'),
(24, 'EU8259577', 'TA2722468', '09:00:00', '18:00:00'),
(25, 'EU3750103', 'TA3588315', '10:00:00', '18:00:00'),
(26, 'EU6054431', 'TA1217256', '09:00:00', '18:00:00'),
(27, 'EU4477511', 'TA0936898', '10:00:00', '18:00:00'),
(28, 'EU8502442', 'TA8885394', '10:00:00', '18:00:00'),
(29, 'EU1090668', 'TA7105214', '10:00:00', '18:00:00'),
(30, 'EU1489516', 'TA2494627', '10:00:00', '18:00:00');

--
-- Inserting data into table correos
--
INSERT INTO correos(id, idEstudiante, correo, contrasena) VALUES
(1, 'EU1703004', 'Brantley@example.com', '12849390M'),
(2, 'EU2040107', 'LalaHough@nowhere.com', '12r49390M'),
(3, 'EU8660850', 'xglzwedk.kovibhvvd@example.com','62288hJ@4'),
(4, 'EU1226513', 'AdellAbreu@example.com','82829920@7'),
(5, 'EU4662638', 'WillardAiello@nowhere.com','62828923J'),
(6, 'EU0073462', 'DollyMares67@example.com','82829292o'),
(7, 'EU0018912', 'Arroyo@nowhere.com','y72823939','12849390M'),
(8, 'EU9855132', 'AlesiaLilly47@nowhere.com','17849390M'),
(9, 'EU4629155', 'FrederickLandis@example.com','12949390M'),
(10, 'EU2522398', 'qfaozsv350@example.com','1284939iM'),
(11, 'EU9473924', 'AbeSSims856@example.com','12844390M'),
(12, 'EU4832093', 'lhonzjm1591@example.com','42849390M'),
(13, 'EU3202694', 'Carranza811@example.com','12847390M'),
(14, 'EU3739178', 'hrzd6@example.com','12849990M'),
(15, 'EU6191317', 'YeseniaMyers536@nowhere.com','1o849390M'),
(16, 'EU2408099', 'Alexander.Albers@nowhere.com','12849390M'),
(17, 'EU8203469', 'EarnestAndres453@example.com','12849390M'),
(18, 'EU3337198', 'AddisonS51@example.com','12849390M'),
(19, 'EU1270622', 'Oralia_Sousa6@example.com','12849390M'),
(20, 'EU5326858', 'dmeoo8@nowhere.com','12849390M'),
(21, 'EU8602252', 'Bolin641@example.com','12849390M'),
(22, 'EU9828307', 'Stack837@nowhere.com','12849390M'),
(23, 'EU1785651', 'Bennett_J.Maldonado58@example.com','12849390M'),
(24, 'EU8259577', 'Milburn396@example.com','12849390M'),
(25, 'EU3750103', 'Peek26@nowhere.com','12849390M'),
(26, 'EU6054431', 'Alston13@nowhere.com','12849390M'),
(27, 'EU4477511', 'TiffanyM.Cunningham62@example.com','12849390M'),
(28, 'EU8502442', 'Rubin432@example.com','12849390M'),
(29, 'EU1090668', 'Hammett@example.com','12849390M'),
(30, 'EU1489516', 'Vernon_Estrada24@nowhere.com','12849390M');

--
-- Inserting data into table exponenconferencias
--
INSERT INTO exponenconferencias(id, idConferencia, idExpositor) VALUES
(1, 'CON417791', 'EX4228097'),
(2, 'CON704273', 'EX2545082'),
(3, 'CON313035', 'EX8623453'),
(4, 'CON227442', 'EX7526182'),
(5, 'CON421099', 'EX5344568'),
(6, 'CON467223', 'EX1660128'),
(7, 'CON896791', 'EX9733187'),
(8, 'CON330283', 'EX6801815'),
(9, 'CON413161', 'EX2242041'),
(10, 'CON625294', 'EX5501805'),
(11, 'CON725127', 'EX8146564'),
(12, 'CON193401', 'EX4002198'),
(13, 'CON750780', 'EX5317572'),
(14, 'CON311969', 'EX2042992'),
(15, 'CON835816', 'EX1985817'),
(16, 'CON659644', 'EX3354128'),
(17, 'CON182002', 'EX2502572'),
(18, 'CON929010', 'EX7371766'),
(19, 'CON335701', 'EX4783268'),
(20, 'CON607835', 'EX0767947'),
(21, 'CON209656', 'EX9662806'),
(22, 'CON519917', 'EX8017463'),
(23, 'CON182327', 'EX0548164'),
(24, 'CON887858', 'EX8483380'),
(25, 'CON349663', 'EX5769488'),
(26, 'CON443676', 'EX5854236'),
(27, 'CON658884', 'EX3616273'),
(28, 'CON386190', 'EX0316562'),
(29, 'CON226158', 'EX1116948'),
(30, 'CON804461', 'EX7278003');

--
-- Inserting data into table impartenconferencias
--
INSERT INTO impartenconferencias(id, idConferencia, idTema) VALUES
(1, 'CON417791', 'TE9553119'),
(2, 'CON704273', 'TE1149240'),
(3, 'CON313035', 'TE1791152'),
(4, 'CON227442', 'TE1578791'),
(5, 'CON421099', 'TE8900866'),
(6, 'CON467223', 'TE4811453'),
(7, 'CON896791', 'TE1043394'),
(8, 'CON330283', 'TE7554912'),
(9, 'CON413161', 'TE4794169'),
(10, 'CON625294', 'TE0164248'),
(11, 'CON725127', 'TE2303955'),
(12, 'CON193401', 'TE1735561'),
(13, 'CON750780', 'TE3400186'),
(14, 'CON311969', 'TE1947905'),
(15, 'CON835816', 'TE2531375'),
(16, 'CON659644', 'TE7374640'),
(17, 'CON182002', 'TE5025636'),
(18, 'CON929010', 'TE9793586'),
(19, 'CON335701', 'TE1311443'),
(20, 'CON607835', 'TE6759697'),
(21, 'CON209656', 'TE1187460'),
(22, 'CON519917', 'TE0311120'),
(23, 'CON182327', 'TE2530035'),
(24, 'CON887858', 'TE5273866'),
(25, 'CON349663', 'TE2651882'),
(26, 'CON443676', 'TE4649707'),
(27, 'CON658884', 'TE7798916'),
(28, 'CON386190', 'TE7756472'),
(29, 'CON226158', 'TE8913392'),
(30, 'CON804461', 'TE8020269');


--
-- Inserting data into table impartentalleres
--
INSERT INTO impartentalleres(id, idProfesor, idTaller) VALUES
(1, 'PR7859653', 'TA3388732'),
(2, 'PR9642799', 'TA3955792'),
(3, 'PR3530459', 'TA8023762'),
(4, 'PR5745229', 'TA6780429'),
(5, 'PR4157895', 'TA7044200'),
(6, 'PR0301251', 'TA8903534'),
(7, 'PR5262142', 'TA0441615'),
(8, 'PR7486918', 'TA2852440'),
(9, 'PR2779524', 'TA8280130'),
(10, 'PR0414120', 'TA4755328'),
(11, 'PR1397132', 'TA2025627'),
(12, 'PR6761699', 'TA0845193'),
(13, 'PR9767130', 'TA0909196'),
(14, 'PR9750873', 'TA1403555'),
(15, 'PR0533981', 'TA8985969'),
(16, 'PR5099859', 'TA8489496'),
(17, 'PR5651003', 'TA0526006'),
(18, 'PR3167416', 'TA6359644'),
(19, 'PR2487774', 'TA7174068'),
(20, 'PR0551487', 'TA3779527'),
(21, 'PR8742652', 'TA1320590'),
(22, 'PR1574194', 'TA7551990'),
(23, 'PR9493470', 'TA4330881'),
(24, 'PR1105377', 'TA2722468'),
(25, 'PR7946000', 'TA3588315'),
(26, 'PR7301479', 'TA1217256'),
(27, 'PR2539800', 'TA0936898'),
(28, 'PR9887070', 'TA8885394'),
(29, 'PR0561897', 'TA7105214'),
(30, 'PR4483160', 'TA2494627');