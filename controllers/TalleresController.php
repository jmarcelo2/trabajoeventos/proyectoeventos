<?php

namespace app\controllers;

use app\models\Talleres;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TalleresController implements the CRUD actions for Talleres model.
 */
class TalleresController extends Controller
{
    /**
     * @inheritDoc
     */
       public function init()
    {
        parent::init();

        // Habilitar CORS
        \Yii::$app->response->headers->set('Access-Control-Allow-Origin', '*');
        \Yii::$app->response->headers->set('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        \Yii::$app->response->headers->set('Access-Control-Allow-Headers', 'Content-Type');
    }
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Talleres models.
     *
     * @return string
     */
    public function actionIndex()
    {
        /*$dataProvider = new ActiveDataProvider([
            'query' => Talleres::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigoTaller' => SORT_DESC,
                ]
            ],
            */
        //]);*/

       /* return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]); */
        $talleres = Talleres::find()->all();
    return Json::encode($talleres);
    }

    /**
     * Displays a single Talleres model.
     * @param string $codigoTaller Codigo Taller
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigoTaller)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigoTaller),
        ]);
    }

    /**
     * Creates a new Talleres model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Talleres();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigoTaller' => $model->codigoTaller]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Talleres model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $codigoTaller Codigo Taller
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigoTaller)
    {
        $model = $this->findModel($codigoTaller);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigoTaller' => $model->codigoTaller]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Talleres model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $codigoTaller Codigo Taller
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigoTaller)
    {
        $this->findModel($codigoTaller)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Talleres model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $codigoTaller Codigo Taller
     * @return Talleres the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigoTaller)
    {
        if (($model = Talleres::findOne(['codigoTaller' => $codigoTaller])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
