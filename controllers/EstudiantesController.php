<?php

namespace app\controllers;

use app\models\Estudiantes;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EstudiantesController implements the CRUD actions for Estudiantes model.
 */
class EstudiantesController extends Controller
{
    
      public function init()
    {
        parent::init();

        // Habilitar CORS
        \Yii::$app->response->headers->set('Access-Control-Allow-Origin', '*');
        \Yii::$app->response->headers->set('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        \Yii::$app->response->headers->set('Access-Control-Allow-Headers', 'Content-Type');
    }
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Estudiantes models.
     *
     * @return string
     */
    public function actionIndex()
    {
        
       /* $dataProvider = new ActiveDataProvider([
            'query' => Estudiantes::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigoEstudiante' => SORT_DESC,
                ]
            ],
            */
        //]);

        /*return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);*/
         $estudiantes = Estudiantes::find()->all();
    return Json::encode($estudiantes);
    }

    /**
     * Displays a single Estudiantes model.
     * @param string $codigoEstudiante Codigo Estudiante
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigoEstudiante)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigoEstudiante),
        ]);
    }

    /**
     * Creates a new Estudiantes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Estudiantes();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigoEstudiante' => $model->codigoEstudiante]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Estudiantes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $codigoEstudiante Codigo Estudiante
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigoEstudiante)
    {
        $model = $this->findModel($codigoEstudiante);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigoEstudiante' => $model->codigoEstudiante]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Estudiantes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $codigoEstudiante Codigo Estudiante
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigoEstudiante)
    {
        $this->findModel($codigoEstudiante)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Estudiantes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $codigoEstudiante Codigo Estudiante
     * @return Estudiantes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigoEstudiante)
    {
        if (($model = Estudiantes::findOne(['codigoEstudiante' => $codigoEstudiante])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
