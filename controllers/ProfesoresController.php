<?php

namespace app\controllers;

use app\models\Profesores;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProfesoresController implements the CRUD actions for Profesores model.
 */
class ProfesoresController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Profesores models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Profesores::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigoProfesor' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Profesores model.
     * @param string $codigoProfesor Codigo Profesor
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigoProfesor)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigoProfesor),
        ]);
    }

    /**
     * Creates a new Profesores model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Profesores();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigoProfesor' => $model->codigoProfesor]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Profesores model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $codigoProfesor Codigo Profesor
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigoProfesor)
    {
        $model = $this->findModel($codigoProfesor);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigoProfesor' => $model->codigoProfesor]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Profesores model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $codigoProfesor Codigo Profesor
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigoProfesor)
    {
        $this->findModel($codigoProfesor)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Profesores model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $codigoProfesor Codigo Profesor
     * @return Profesores the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigoProfesor)
    {
        if (($model = Profesores::findOne(['codigoProfesor' => $codigoProfesor])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
