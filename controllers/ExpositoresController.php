<?php

namespace app\controllers;

use app\models\Expositores;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ExpositoresController implements the CRUD actions for Expositores model.
 */
class ExpositoresController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Expositores models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Expositores::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigoExpositor' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Expositores model.
     * @param string $codigoExpositor Codigo Expositor
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigoExpositor)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigoExpositor),
        ]);
    }

    /**
     * Creates a new Expositores model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Expositores();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigoExpositor' => $model->codigoExpositor]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Expositores model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $codigoExpositor Codigo Expositor
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigoExpositor)
    {
        $model = $this->findModel($codigoExpositor);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigoExpositor' => $model->codigoExpositor]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Expositores model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $codigoExpositor Codigo Expositor
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigoExpositor)
    {
        $this->findModel($codigoExpositor)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Expositores model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $codigoExpositor Codigo Expositor
     * @return Expositores the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigoExpositor)
    {
        if (($model = Expositores::findOne(['codigoExpositor' => $codigoExpositor])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
