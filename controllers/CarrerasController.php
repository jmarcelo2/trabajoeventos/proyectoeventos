<?php

namespace app\controllers;

use app\models\Carreras;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CarrerasController implements the CRUD actions for Carreras model.
 */
class CarrerasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Carreras models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Carreras::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigoCarrera' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Carreras model.
     * @param string $codigoCarrera Codigo Carrera
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigoCarrera)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigoCarrera),
        ]);
    }

    /**
     * Creates a new Carreras model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Carreras();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigoCarrera' => $model->codigoCarrera]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Carreras model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $codigoCarrera Codigo Carrera
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigoCarrera)
    {
        $model = $this->findModel($codigoCarrera);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigoCarrera' => $model->codigoCarrera]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Carreras model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $codigoCarrera Codigo Carrera
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigoCarrera)
    {
        $this->findModel($codigoCarrera)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Carreras model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $codigoCarrera Codigo Carrera
     * @return Carreras the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigoCarrera)
    {
        if (($model = Carreras::findOne(['codigoCarrera' => $codigoCarrera])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
