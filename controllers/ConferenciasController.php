<?php

namespace app\controllers;

use app\models\Conferencias;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ConferenciasController implements the CRUD actions for Conferencias model.
 */
class ConferenciasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Conferencias models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Conferencias::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigoConferencia' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Conferencias model.
     * @param string $codigoConferencia Codigo Conferencia
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigoConferencia)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigoConferencia),
        ]);
    }

    /**
     * Creates a new Conferencias model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Conferencias();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigoConferencia' => $model->codigoConferencia]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Conferencias model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $codigoConferencia Codigo Conferencia
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigoConferencia)
    {
        $model = $this->findModel($codigoConferencia);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigoConferencia' => $model->codigoConferencia]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Conferencias model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $codigoConferencia Codigo Conferencia
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigoConferencia)
    {
        $this->findModel($codigoConferencia)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Conferencias model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $codigoConferencia Codigo Conferencia
     * @return Conferencias the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigoConferencia)
    {
        if (($model = Conferencias::findOne(['codigoConferencia' => $codigoConferencia])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
